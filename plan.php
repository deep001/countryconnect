﻿<?php
include_once __DIR__ ."/autoload/define.php";
include_once CONFIG_PATH .'/config.php';
include_once CLASS_PATH . '/class.headers.php';


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<?php include_once INCLUDE_PATH ."/head.php"; ?>

    <style>
        /* PRICING TABLES */
        .spacer {
            height: 50px;
        }

        .peering {
            color: #00989b !important;
        }

        .no-space {
            margin-left: 0;
        }

            .no-space [class*="span"] {
                margin-left: 0;
            }

        .row-fluid.no-space [class*="span"] {
            margin-left: 0;
        }

        .row-fluid.no-space .span1 {
            width: 8.334% !important;
        }

        .row-fluid.no-space .span2 {
            width: 16.667% !important;
        }

        .row-fluid.no-space .span3 {
            width: 25% !important;
        }

        .row-fluid.no-space .span4 {
            width: 33.334% !important;
        }

        .row-fluid.no-space .span5 {
            width: 41.667% !important;
        }

        .row-fluid.no-space .span6 {
            width: 50% !important;
        }

        .row-fluid.no-space .span7 {
            width: 58.333% !important;
        }

        .row-fluid.no-space .span8 {
            width: 66.666% !important;
        }

        .row-fluid.no-space .span9 {
            width: 75% !important;
        }

        .row-fluid.no-space .span10 {
            width: 83.334% !important;
        }

        .row-fluid.no-space .span11 {
            width: 91.666% !important;
        }

        .row-fluid.no-space .span12 {
            width: 100% !important;
        }

        @media (max-width: 767px) {
            .row-fluid.no-space [class*="span"] {
                width: auto !important;
            }
        }

        .pricing_table {
            text-align: center;
            margin: 25px 0;
        }

            .pricing_table .tb_header {
                background-color: #8AB623;
            }

                .pricing_table .tb_header h4 {
                    margin: 0;
                    padding: 15px 0;
                    border-bottom: 3px solid #5E852C;
                    color: white;
                    font-size: 20px;
                    text-shadow: 1px 1px 0 rgba(0,0,0,0.3);
                    font-weight: 700;
                    border-top-left-radius: 3px;
                    border-top-right-radius: 4px;
                }

            .pricing_table .price {
                margin: 0;
                padding: 15px 0;
                border-bottom: 2px solid white;
                font-size: 46px;
                color: #fff;
                text-shadow: 1px 1px 0 black;
                font-weight: 900;
                letter-spacing: 0;
                line-height: 1;
            }

                .pricing_table .price p:first-letter {
                    font-weight: 300;
                    margin-right: 2px;
                    font-size: 26px;
                }

                .pricing_table .price p {
                    margin: 0;
                }

                    .pricing_table .price p span {
                        display: block;
                        padding: 4px;
                        font-size: 14px;
                        font-weight: 500;
                    }

            .pricing_table .tb_content {
                margin: 0;
                padding: 0;
                list-style: none;
            }

                .pricing_table .tb_content li {
                    margin: 0;
                    padding: 5px 0;
                    font-size: 12px;
                    line-height: 2;
                }

                    .pricing_table .tb_content li:nth-child(odd) {
                        background-color: rgba(0,0,0,0.1);
                    }

                    .pricing_table .tb_content li:first-child {
                        border-top: 1px solid white;
                    }

                    .pricing_table .tb_content li hr {
                        margin: 10px 50px;
                    }

            .pricing_table .signin {
                padding: 15px 0 15px;
                background: rgba(0, 0, 0, 0.15);
                border-top: 3px solid #5E852C;
            }

            .pricing_table .pr_table_col {
                background: #fff;
                position: relative;
                z-index: 0;
                box-shadow: 0 0 8px rgba(0,0,0,0.1);
            }

            .pricing_table [class*="span"]:nth-child(odd) .pr_table_col {
                background-color: #F1F1F1;
            }

            .pricing_table .pr_table_col.highlight {
                z-index: 1;
            }

            .pricing_table .pr_table_col:hover {
                z-index: 2;
            }

            .pricing_table .pr_table_col.highlight,
            .pricing_table .pr_table_col:hover {
                margin-top: -15px;
                box-shadow: 0 0 30px rgba(0, 0, 0, 0.5);
            }

                .pricing_table .pr_table_col.highlight .tb_content li,
                .pricing_table .pr_table_col:hover .tb_content li {
                    padding: 7px 0;
                }

                .pricing_table .pr_table_col.highlight .price,
                .pricing_table .pr_table_col:hover .price {
                    padding: 20px 0;
                }

            .pricing_table .pr_table_col {
                -webkit-transition: box-shadow 0.2s ease-out;
                transition: box-shadow 0.2s ease-out;
            }

                .pricing_table .pr_table_col.caption_column {
                    margin-top: 50px;
                    box-shadow: none;
                }

                    .pricing_table .pr_table_col.caption_column .tb_header {
                        font-size: 18px;
                        padding: 41px 0;
                        color: #444;
                        font-weight: 700;
                        border-bottom: 2px solid #fff;
                        background: #eee;
                        text-shadow: 1px 1px 0 #fff;
                    }

                    .pricing_table .pr_table_col.caption_column .tb_content li {
                        padding: 5px 0;
                        text-align: left;
                        text-indent: 30px;
                        background: #eee;
                    }
        /* pricing table skins */
        .pr_table_col[data-color=red] .tb_header h4 {
            border-bottom: 3px solid #941414;
        }

        .pr_table_col[data-color=red] .signin {
            border-top: 3px solid #941414;
        }

        .pr_table_col[data-color=red] .tb_header {
            background-color: #CD2122;
        }

        .pr_table_col[data-color=blue] .tb_header h4 {
            border-bottom: 3px solid #345370;
        }

        .pr_table_col[data-color=blue] .signin {
            border-top: 3px solid #345370;
        }

        .pr_table_col[data-color=blue] .tb_header {
            background-color: #2A8FBD;
        }

        .pr_table_col[data-color=green] .tb_header h4 {
            border-bottom: 3px solid #5E852C;
        }

        .pr_table_col[data-color=green] .signin {
            border-top: 3px solid #5E852C;
        }

        .pr_table_col[data-color=green] .tb_header {
            background-color: #8AB623;
        }

        .pr_table_col[data-color=turquoise] .tb_header h4 {
            border-bottom: 3px solid #0A817F;
        }

        .pr_table_col[data-color=turquoise] .signin {
            border-top: 3px solid #0A817F;
        }

        .pr_table_col[data-color=turquoise] .tb_header {
            background-color: #12C6C2;
        }

        .pr_table_col[data-color=orange] .tb_header h4 {
            border-bottom: 3px solid #AF3F08;
        }

        .pr_table_col[data-color=orange] .signin {
            border-top: 3px solid #AF3F08;
        }

        .pr_table_col[data-color=orange] .tb_header {
            background-color: #EB540A;
        }

        .pr_table_col[data-color=purple] .tb_header h4 {
            border-bottom: 3px solid #630C72;
        }

        .pr_table_col[data-color=purple] .signin {
            border-top: 3px solid #630C72;
        }

        .pr_table_col[data-color=purple] .tb_header {
            background-color: #8A2D9A;
        }

        .pr_table_col[data-color=yellow] .tb_header h4 {
            border-bottom: 3px solid #A8A80A;
        }

        .pr_table_col[data-color=yellow] .signin {
            border-top: 3px solid #A8A80A;
        }

        .pr_table_col[data-color=yellow] .tb_header {
            background-color: #D6D609;
        }

        .pr_table_col[data-color=green_lemon] .tb_header h4 {
            border-bottom: 3px solid #708D0B;
        }

        .pr_table_col[data-color=green_lemon] .signin {
            border-top: 3px solid #708D0B;
        }

        .pr_table_col[data-color=green_lemon] .tb_header {
            background-color: #9CC607;
        }

        .pr_table_col[data-color=dark] .tb_header h4 {
            border-bottom: 3px solid #000;
        }

        .pr_table_col[data-color=dark] .signin {
            border-top: 3px solid #000;
        }

        .pr_table_col[data-color=dark] .tb_header {
            background-color: #333;
        }

        .pr_table_col[data-color=light] .tb_header h4 {
            border-bottom: 3px solid #747474;
        }

        .pr_table_col[data-color=light] .signin {
            border-top: 3px solid #747474;
        }

        .pr_table_col[data-color=light] .tb_header {
            background-color: #aaa;
        }

        .pricing_table.rounded-corners [class*="span"]:first-child .tb_header {
            border-top-left-radius: 5px;
        }

        .pricing_table.rounded-corners [class*="span"]:last-child .tb_header {
            border-top-right-radius: 5px;
        }

        .pricing_table.rounded-corners .pr_table_col:hover .tb_header,
        .pricing_table.rounded-corners .highlight .tb_header {
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .pricing_table.rounded-corners .pr_table_col:hover,
        .pricing_table.rounded-corners .highlight {
            border-radius: 5px;
        }

            .pricing_table.rounded-corners .pr_table_col:hover .signin,
            .pricing_table.rounded-corners .highlight .signin {
                border-bottom-left-radius: 5px;
                border-bottom-right-radius: 5px;
            }

        .pricing_table.rounded-corners [class*="span"]:last-child .tb_header {
            border-top-right-radius: 5px;
        }

        .pricing_table.rounded-corners [class*="span"]:first-child .pr_table_col,
        .pricing_table.rounded-corners [class*="span"]:first-child .signin {
            border-bottom-left-radius: 5px;
        }

        .pricing_table.rounded-corners [class*="span"]:last-child .pr_table_col,
        .pricing_table.rounded-corners [class*="span"]:last-child .signin {
            border-bottom-right-radius: 5px;
        }


        /* Pricing Tables element (new style) */

        .pricing-table-element {
            *zoom: 1;
            background: #fff;
            border: 1px solid #e6e7e8;
            border-radius: 4px;
            font-size: 13px;
            color: #999999;
        }

            .pricing-table-element::before, .pricing-table-element::after {
                display: table;
                line-height: 0;
                content: "";
            }

            .pricing-table-element::after {
                clear: both;
            }

            .pricing-table-element ul {
                list-style: none;
                margin: 0;
                padding: 0;
                display: table;
                width: 100%;
            }

            .pricing-table-element .features-column {
                background: #eee;
                border-radius: 4px 0 0 4px;
                text-align: right;
                width: 28%;
            }

                .pricing-table-element .features-column ul li {
                    border-bottom: 1px solid #e6e7e8;
                    padding: 15px 40px 15px 0;
                    height: 44px;
                    line-height: 1;
                }

            .pricing-table-element .plan-column,
            .pricing-table-element .features-column {
                float: left;
            }

            .pricing-table-element[data-columns="2"] .plan-column {
                width: 50%;
            }

            .pricing-table-element[data-columns="3"] .plan-column {
                width: 33.3333%;
            }

            .pricing-table-element[data-columns="4"] .plan-column {
                width: 25%;
            }

            .pricing-table-element[data-columns="5"] .plan-column {
                width: 20%;
            }

            .pricing-table-element[data-columns="1"] .features-column ~ .plan-column {
                width: 72%;
            }
            /* with features-col */
            .pricing-table-element[data-columns="2"] .features-column ~ .plan-column {
                width: -webkit-calc(72% / 2);
                width: calc(72% / 2);
            }
            /* with features-col */
            .pricing-table-element[data-columns="3"] .features-column ~ .plan-column {
                width: -webkit-calc(72% / 3);
                width: calc(72% / 3);
            }
            /* with features-col */
            .pricing-table-element[data-columns="4"] .features-column ~ .plan-column {
                width: -webkit-calc(72% / 4);
                width: calc(72% / 4);
            }
            /* with features-col */
            .pricing-table-element[data-columns="5"] .features-column ~ .plan-column {
                width: -webkit-calc(72% / 5);
                width: calc(72% / 5);
            }
            /* with features-col */
            .pricing-table-element .plan-column {
                border-left: 1px solid #e6e7e8;
            }

                .pricing-table-element .plan-column:first-child {
                    border-left: 0;
                }

                .pricing-table-element .plan-column ul {
                    display: table;
                }

                    .pricing-table-element .plan-column ul li {
                        display: table-row;
                    }

                .pricing-table-element .plan-column .inner-cell {
                    border-bottom: 1px solid #00989b;
                    padding: 0;
                    text-align: center;
                    height: 50px;
                    line-height: 1;
                    display: table-cell;
                    vertical-align: middle;
                    font-size: 1.1em;
                    color: #5f5f5f;
                }

                .pricing-table-element .plan-column ul li:last-child .inner-cell {
                    height: 100px;
                }

                .pricing-table-element .plan-column ul li:nth-child(odd):not(:first-child) {
                    background: #fafbfb;
                }

                .pricing-table-element .plan-column .plan-title {
                    color: #888888;
                    font-size: 25px;
                    height: auto;
                    padding: 0;
                }

            .pricing-table-element .plan-title .inner-cell {
                height: 75px;
            }

            .pricing-table-element .plan-column .subscription-price {
                font-size: 13px;
                color: #999999;
                height: auto;
                padding: 0;
            }

            .pricing-table-element .subscription-price .inner-cell {
                height: 120px;
            }

            .pricing-table-element .plan-column .subscription-price .currency {
                font-size: 36px;
                color: #5f5f5f;
                font-weight: 700;
                vertical-align: bottom;
                display: inline-block;
                line-height: 1.5;
                margin-right: 2px;
            }

            .pricing-table-element .plan-column .subscription-price .price {
                font-size: 45px;
                color: #5f5f5f;
                font-weight: 700;
                line-height: 1;
            }

                .pricing-table-element .plan-column .subscription-price .price::after {
                    content: "\a";
                    white-space: pre;
                }

            .pricing-table-element .plan-column.featured {
                border-radius: 4px;
                box-shadow: 0 0 10px 4px rgba(0, 0, 0, 0.06);
                position: relative;
                margin-top: -20px;
                background: #fff;
                margin: -20px 0;
            }

        @media only screen and (min-width: 992px) {
            .pricing-table-element .plan-column.featured {
                margin: -20px;
            }

            .pricing-table-element[data-columns="1"] .plan-column.featured {
                width: -webkit-calc(100% + 40px);
                width: calc(100% + 40px);
            }

            .pricing-table-element[data-columns="2"] .plan-column.featured {
                width: -webkit-calc(50% + 40px);
                width: calc(50% + 40px);
            }

            .pricing-table-element[data-columns="3"] .plan-column.featured {
                width: -webkit-calc(33.3333% + 40px);
                width: calc(33.3333% + 40px);
            }

            .pricing-table-element[data-columns="4"] .plan-column.featured {
                width: -webkit-calc(25% + 40px);
                width: calc(25% + 40px);
            }

            .pricing-table-element[data-columns="5"] .plan-column.featured {
                width: -webkit-calc(20% + 40px);
                width: calc(20% + 40px);
            }
        }

        .pricing-table-element .plan-column.featured + .plan-column {
            border-left-color: transparent;
        }

        .pricing-table-element .plan-column.featured .plan-title {
            padding: 0;
            color: #45474d;
        }

            .pricing-table-element .plan-column.featured .plan-title .inner-cell {
                height: 95px;
            }

                .pricing-table-element .plan-column.featured .plan-title .inner-cell::after {
                    content: 'MOST POPULAR';
                    opacity: .4;
                    font-size: 12px;
                    font-weight: 600;
                    line-height: 20px;
                    display: block;
                }

        .pricing-table-element .plan-column.featured .subscription-price .inner-cell {
            background: #901e78;
            ;
            color: #fff;
        }

        .pricing-table-element .plan-column.featured .subscription-price span {
            color: #fff;
        }

        .pricing-table-element .plan-column.featured ul li:last-child .inner-cell {
            height: 120px;
        }

        @media only screen and (max-width: 991px) {
            .pricing-table-element .features-column.hidesm {
                display: none;
            }

            .pricing-table-element[data-columns="1"] .features-column ~ .plan-column {
                width: 100%;
            }
            /* with features-col */
            .pricing-table-element[data-columns="2"] .features-column ~ .plan-column {
                width: -webkit-calc(100% / 2);
                width: calc(100% / 2);
            }
            /* with features-col */
            .pricing-table-element[data-columns="3"] .features-column ~ .plan-column {
                width: -webkit-calc(100% / 3);
                width: calc(100% / 3);
            }
            /* with features-col */
            .pricing-table-element[data-columns="4"] .features-column ~ .plan-column {
                width: -webkit-calc(100% / 4);
                width: calc(100% / 4);
            }
            /* with features-col */
            .pricing-table-element[data-columns="5"] .features-column ~ .plan-column {
                width: -webkit-calc(100% / 5);
                width: calc(100% / 5);
            }
            /* with features-col */
            .pricing-table-element .plan-column.featured {
                margin: -20px 0;
            }
        }

        @media only screen and (max-width: 767px) {
            .pricing-table-element .plan-column {
                width: 100% !important;
            }

                .pricing-table-element .plan-column:not(.featured) {
                    border: solid #cdcdcd;
                    border-width: 3px 0;
                }

                .pricing-table-element .plan-column.featured {
                    margin-bottom: 0;
                }
        }

        /* process boxes */
        .process_box {
            position: relative;
            padding: 0;
            margin-bottom: 80px;
            box-shadow: none;
        }

            .process_box:after {
                border-color: transparent;
                border-top-color: #eee;
                border-width: 10px;
                margin-left: -10px;
                left: 10%;
            }

            .process_box:before {
                border-color: transparent;
                border-top-color: #ccc;
                border-width: 13px;
                margin-left: -13px;
                left: 10%;
            }

            .process_box[data-align=right]:after,
            .process_box[data-align=right]:before {
                right: 10%;
                left: auto;
                margin-left: 0;
            }

            .process_box[data-align=right]:before {
                margin-right: -13px;
            }

            .process_box[data-align=center]:after,
            .process_box[data-align=center]:before {
                left: 50%;
            }

            .process_box[data-align=center]:after {
                border-top-color: #fff;
            }

            .process_box .number {
                padding: 0;
                height: 100%;
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 240px;
                text-align: center;
                float: left;
            }

                .process_box .number span {
                    font-size: 100px;
                    font-weight: 900;
                    color: #333;
                    vertical-align: middle;
                    position: relative;
                    top: 50%;
                    line-height: 1;
                    display: block;
                }

            .process_box .content {
                margin-left: 240px;
                padding: 25px 35px 20px 35px;
                float: left;
                font-size: 14px;
                line-height: 30px;
                color: #929292;
                background-color: #fff;
            }

            .process_box .stp_title {
                font-size: 14px;
                text-transform: uppercase;
                font-weight: bold;
                color: #333;
            }

            .process_box[data-align=right] .number {
                left: auto;
                right: 0;
                border-right: 0;
            }

            .process_box[data-align=right] .content {
                margin-left: auto;
                margin-right: 240px;
                padding: 25px 35px 20px 35px;
                color: #929292;
                line-height: 30px;
                font-size: 14px;
            }

        @media (min-width: 768px) {
            .process_box .number span {
                transform: translateY(-50%);
                -webkit-transform: translateY(-50%);
            }

            .process_box[data-align=right] .content {
                text-align: right;
            }

            .process_box .content:before, .process_box .content:after {
                -webkit-transition: all .25s ease-in-out;
                transition: all .25s ease-in-out;
            }

            .process_box .content:before {
                position: absolute;
                content: '';
                top: 0;
                width: 2px;
                height: 100%;
                background-color: #cd2122;
            }

            .process_box[data-align=left] .content:before {
                left: 240px;
            }

            .process_box[data-align=right] .content:before {
                right: 240px;
            }

            .process_box:hover .content:before {
                width: 5px;
            }

            .process_box .content:after {
                position: absolute;
                content: '';
                border: 14px solid;
                border-color: transparent;
                top: 50%;
                margin-top: -14px;
            }

            .process_box[data-align=left] .content:after {
                left: 242px;
                border-left-color: #cd2122;
            }

            .process_box[data-align=right] .content:after {
                right: 242px;
                border-right-color: #cd2122;
            }

            .process_box[data-align=left]:hover .content:after {
                left: 245px;
            }

            .process_box[data-align=right]:hover .content:after {
                right: 245px;
            }

            .process_box:after, .process_box .number:before, .process_box .number:after {
                background-color: #F1F1F1 !important;
            }

            .process_box:after {
                position: absolute;
                content: '';
                width: calc(100% - 234px);
                height: 6px;
                bottom: -43px;
            }

            .process_box[data-align=left]:after {
                left: 130px;
            }

            .process_box[data-align=right]:after {
                right: 114px;
            }

            .process_box .number:before, .process_box .number:after {
                position: absolute;
                content: '';
                width: 6px;
                height: 50px;
            }

            .process_box .number:before {
                top: -37px;
            }

            .process_box .number:after {
                bottom: -37px;
            }

            .process_box:first-child .number:before, .process_box.last .number:after, .process_box.last:after {
                content: none;
            }

            .process_box.last {
                margin-bottom: 0;
            }
        }



        .plans {
           text-align: center;
    background-color: #fff;
    padding: 3%;
    box-shadow: 0px 0px 2px 1px rgb(202, 198, 198);
    margin-top: 10%;
    margin-left: 5%;
        }

            .plans h4 {
                color: #777777;
            }




        .mbps {
            font-size: 1.4em;
            font-weight: 700;
            color: #901d78;
        }
    </style>



</head>

<body>


<?php include_once INCLUDE_PATH ."/header.php"; ?>








    <header class="page-header">

<div class="container-fluid">

<img src="images/plan.jpg" alt="Unlimited broadband internet plans Delhi" class="img-responsive"  />

</div>


</header>


    <div class="planbg">
        <div class="container">
            <h2>ZEONET BRAND OFFERINGS</h2>
            <!--col6 end-->

            <div class="col-md-12 col-sm-12">
                <div class="col-md-12 col-sm-12">
                    <p>
                        Considering the fact every customer has different requirements, we have produced five different internet plans.. 
                    Connect with us to  enjoy following benefits of the Zeonet:
                    </p>
                    <ul class="sitelist">

                        <li>Fast adoption &amp; hosting of internet based applications</li>
                        <li>Dedicated &amp; Symmetric Bandwidth with low latency</li>
                        <li>Smooth Proceeding of you Online Business</li>
                        <li>24*7 customer support to address your queries or complaints</li>
                        <li>Their prices might vary depending upon your location. </li>
                    </ul>



                </div>


                

            </div>
            <!--col6 end-->
        </div>
    </div>
    <!--planbg end-->
    <div class="clearfix"></div>

    <div class="container-fluid" style="background: #fafafa; border-bottom: 1px solid #ddd;">
        <section>
<div class="planbg">
<div class="row">
<h2>HOME PLANS</h2>
<br />

<div class="col-md-10 col-md-offset-5">

<div class="pricing-table-element" data-columns="4">

<table class="table table-condensed">
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active"  >MONTHLY RENTAL</td>
  <td class="success">BROADBAND SPEED</td>
  <td class="warning">DATA</td>
  <td class="danger">AFTER USAGE</td>
  <td class="info">VALIDITY</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.699/</td>
  <td class="success">10 MBPS</td>
  <td class="warning">50 GB</td>
  <td class="danger">512 KBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.899/</td>
  <td class="success">10 MBPS</td>
  <td class="warning">60 GB</td>
  <td class="danger">512 KBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.1199/</td>
  <td class="success">10 MBPS</td>
  <td class="warning">80 GB</td>
  <td class="danger">512 KBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.1499/</td>
  <td class="success">10 MBPS</td>
  <td class="warning">Unlimited</td>
  <td class="danger">----</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.1099/</td>
  <td class="success">15 MBPS</td>
  <td class="warning">60 GB</td>
  <td class="danger">1 MBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.1399/</td>
  <td class="success">15 MBPS</td>
  <td class="warning">80 GB</td>
  <td class="danger">1 MBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.1799/</td>
  <td class="success">15 MBPS</td>
  <td class="warning">100 GB</td>
  <td class="danger">1 MBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.2499/</td>
  <td class="success">15 MBPS</td>
  <td class="warning">Unlimited</td>
  <td class="danger">----</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.1699/</td>
  <td class="success">25 MBPS</td>
  <td class="warning">70 GB</td>
  <td class="danger">2 MBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.1999/</td>
  <td class="success">25 MBPS</td>
  <td class="warning">90 GB</td>
  <td class="danger">2 MBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.2299/</td>
  <td class="success">25 MBPS</td>
  <td class="warning">120 GB</td>
  <td class="danger">2 MBPS</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.2999/</td>
  <td class="success">25 MBPS</td>
  <td class="warning">Unlimited</td>
  <td class="danger">----</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.3599/</td>
  <td class="success">30 MBPS</td>
  <td class="warning">Unlimited</td>
  <td class="danger">----</td>
  <td class="info">30 DAYS</td>
</tr>
</table>



</div>
<br/>
<h2>SME PLANS</h2>

<div class="pricing-table-element" data-columns="4">

<table class="table table-condensed">
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active"  >MONTHLY RENTAL</td>
  <td class="success">BROADBAND SPEED</td>
  <td class="warning">DATA</td>
  <td class="danger">AFTER USAGE</td>
  <td class="info">VALIDITY</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.2499/</td>
  <td class="success">10 MBPS</td>
  <td class="warning">Unlimited</td>
  <td class="danger">----</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.3699/</td>
  <td class="success">15 MBPS</td>
  <td class="warning">Unlimited</td>
  <td class="danger">----</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.4299/</td>
  <td class="success">20 MBPS</td>
  <td class="warning">Unlimited</td>
  <td class="danger">----</td>
  <td class="info">30 DAYS</td>
</tr>
<tr style="font-weight:bold;color:#000;text-align:center;">
  <td class="active">Rs.4899/</td>
  <td class="success">25 MBPS</td>
  <td class="warning">Unlimited</td>
  <td class="danger">----</td>
  <td class="info">30 DAYS</td>
</tr>


</table>



</div>




</div>



</div>

</div>

<br style="clear:both;" />
	
</section>
    </div>

    <div class="clearfix"></div>

    <?php include_once INCLUDE_PATH ."/footer.php"; ?>


    </div><!--containerfluid end-->


    <?php include_once INCLUDE_PATH ."/fscript.php"; ?>
</body>

</html>
