﻿<?php
include_once __DIR__ ."/autoload/define.php";
include_once CONFIG_PATH .'/config.php';
include_once CLASS_PATH . '/class.headers.php';


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<?php include_once INCLUDE_PATH ."/head.php"; ?>

</head>

<body>


<?php include_once INCLUDE_PATH ."/header.php"; ?>










    <header class="page-header">

<div class="container-fluid">

<img src="images/leasedline.jpg" alt="leased line internet services Delhi" class="img-responsive"  />

</div>


</header>


    <div class="planbg">
        <div class="container">
            <h2>Smart Internet Leased Line (ILL)</h2>
            <!--col6 end-->

            <div class="col-md-12 col-sm-12">
                <p>The growing dependence of enterprises on the internet needs constant connectivity to communicate and transact. For smooth work flow, speedy and reliable internet connectivity is a must in this era of globalization. Under such circumstances, premium, secured and cost effective internet solution to run their businesses effectively has become a matter of concern for many.</p>
                <p>
                    It has been observed that shared internet lines with multiple users simultaneously slows down the speed and hence affects the productivity of the company.  Our smart Internet Leased Line service overcomes this common problem of the corporate world by empowering companies with the reliable and constant hi-speed communication.
                    <br />
                    With our high performance and low latency Internet Leased Line (ILL), we are fulfilling the connectivity requirements of B2B and B2C organizations including the companies whose applications are critical and bandwidth are intensive.
                </p>
                <p>
                    Meet your dedicated and shared bandwidth requirements with our SMART LEASED LINE internet services designed exclusively for the industrial sector.
                    <br />
                </p>



                <div class="col-md-12">

                    <div class="col-md-6">
                        <h4>Our Specialities
                        </h4>
                        <p align="justify">We are way different from our competitors in our approach. The dedicated team of professionals do the analysis of our client's business and its requirements and then accordingly suggests a comprehensive solution.  We conduct two-way communication with our clients to give them value for money. </p>
                        <p></p>
                        <ul style="line-height: 1.7;">
                            <li><strong>A Total Focus on Delivering</strong></li>
                            <li><strong>Speed &amp; Reliability</strong></li>
                            <li><strong>No downtime</strong></li>
                            <li><strong>Highly Advanced Infrastructure</strong></li>
                            <li><strong>Underground OFC Backbone</strong></li>
                            <li><strong>100% Non Blocking Throughout</strong></li>
                            <li><strong>Customized SLA</strong></li>
                            <li><strong>Guaranteed bandwidth for critical business usage, (Symmetric 1:1 bandwidth only)</strong></li>
                            <li><strong>Reliable internet connection (24 x 7) with redundancy on domestic link</strong></li>
                            <li><strong>Highly secured architecture with Firewall, IDS (Intrusion Detection System),</strong></li>
                            <li><strong>IPS (Intrusion Prevention Systems), Content filtering etc</strong></li>
                            <li><strong>24 hour network / security monitoring & technical helpdesk</strong></li>
                            <li><strong>Dedicated support team for Corporate Client </strong></li>
                        </ul>
                    </div>


                    <div class="col-md-6">

                        <img src="images/Leasedmap.svg.png" alt="Private leased line network map" class="img-responsive center-block" />


                    </div>
                </div>

                <p>&nbsp;</p>

                <p>&nbsp;</p>


                <p>&nbsp;</p>

                <p>&nbsp;</p>
            </div>
            <!--col6 end-->
        </div>
    </div>
    <!--planbg end-->
    <div class="clearfix"></div>








    <div class="clearfix"></div>


    <?php include_once INCLUDE_PATH ."/footer.php"; ?>


    </div><!--containerfluid end-->


    <?php include_once INCLUDE_PATH ."/fscript.php"; ?>
</body>

</html>
