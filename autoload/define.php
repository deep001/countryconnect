<?php
//error_reporting(1);
define('ROOT',__DIR__.'/..');
define('APP_PATH',ROOT.'/app');
define('CLASS_PATH',APP_PATH.'/class');
define('CONFIG_PATH',APP_PATH.'/config');

define('ASSET_PATH','/app/assets');
//define('ASSET_PATH',APP_PATH. '/assets');
define('INCLUDE_PATH',APP_PATH.'/include');


define('EMAIL_TEMPLATES_PATH',APP_PATH.'/templates');
define('LOGOUT','/logout.php');