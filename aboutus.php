﻿<?php
include_once __DIR__ ."/autoload/define.php";
include_once CONFIG_PATH .'/config.php';
include_once CLASS_PATH . '/class.headers.php';


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include_once INCLUDE_PATH ."/head.php"; ?>

</head>

<body>


<?php include_once INCLUDE_PATH ."/header.php"; ?>










    <header class="page-header">

<div class="container-fluid">

<img src="images/aboutus.jpg" class="img-responsive"/>

</div>


</header>


    <div class="planbg">
        <div class="container">
            <h2>About us</h2>
            <!--col6 end-->

            <div class="col-md-12 col-sm-12">
                <p>&quot; Zeonet is an emerging Internet Service provider (ISP), committed to build the next generation wireless broadband and leased line internet services for domestic and commercial uses.&quot;</p>
                <p>
                    The spirit of expanding the horizon of internet in India at reasonable prices has led to the inception of Zeonet. With instant connectivity, affordable plans, high speed internet and value added services, our company strives to win customers for lifetime. We completely rely on advanced technology (radio and fiber connection) to give seamless services, free from the nuisance of trouble shooting and dropped connections.
                </p>
                <p>
                    Within short duration, we have been able to create a niche for ourselves in the wide spectrum of telecom industry, which gives us immense motivation to be indomitable in our work. Not to forget the dedicated and passionate personnel at Zeonet that helps us to improve day after day, thus enriching our customer's experience.
                    <br />
                    Above all, we stand stall among competitors by facilitating enhanced communication to our wide network of clients at best prices.
                    <br />
                </p>
                <p>&nbsp;</p>
            </div>
            <!--col6 end-->
        </div>
    </div>
    <!--planbg end-->
    <div class="clearfix"></div>


    <div class="planbg">
        <div class="container">


            <h2>WHAT WE DO</h2>


            <div class="col-md-12 col-sm-12">
                <p>Facing problems in video streaming or while broadcasting conference calls? It's time to switch to a new-age telecommunication service which is more reliable, consistent and economical.Since internet needs are not same for commercial and domestic purposes, we have designed five different data plans for our customers. They target the specific needs of distinctive customers to let them enjoy hi-speed, uninterrupted internet for entertainment, business and other online applications.Talk to our experts today to know the best plan for you. </p>
                <p>
                    <strong>SALIENT FEATURES OF OUR SERVICES:</strong><br />
                </p>
                <ul class="sitelist">

                    <li>24×7 Uninterrupted Internet</li>
                    <li>Hi-Speed, Better than Dial-up Modem</li>
                    <li>Instant Connectivity </li>
                    <li>Cost Effective</li>



                    <li>Free From  Trouble Shooting & Dropped Connection</li>
                    <li>Latest Technology </li>
                    <li>Free Set-Up Installation</li>
                </ul>


                <p>

                    <h5><strong>OUR VISION</strong></h5>
                    To encourage the use and improvement of internet services across the nation and beyond for better communication in an economical way
                </p>



                <h5><strong>OUR MISSION </strong></h5>
                <p>
                    To secure a premier position in the new age telecommunication industry
                </p>
                </p>
    <p>&nbsp; </p>
                <h4 class="text-center">BE A PART OF IMPROVED TELECOM COMMUNICATION TO EMBRACE THE BEST IN TECHNOLOGY</h4>

            </div>
            <!--col6 end-->











            <!--col6 end-->
        </div>


    </div>
    <!--planbg end-->
    <div class="clearfix"></div>





    <div class="getintouch">
        <h1>Send Your Query</h1>
        <div class="container">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <input type="text" placeholder="Your Name" class="form-control getinput" />
                </div>

                <div class="form-group">
                    <input type="text" placeholder="Your Email" class="form-control getinput" />
                </div>

                <div class="form-group">
                    <input type="text" placeholder="Your Phone" class="form-control getinput" />
                </div>
            </div>
            <!--col6 end-->

            <div class="col-md-6 col-sm-12">
                <textarea name="" cols="" rows="" class="form-control getarea" placeholder="Your Message"></textarea>
            </div>
            <!--col6 end-->

            <div class="col-md-12 text-center"><a href="#">
                <input type="submit" value="SENT MESSAGE" class="btn btn-primary" /></a></div>
        </div>
        <!--container end-->
    </div>
    <!--getintouch end-->
    <div class="clearfix"></div>


    <?php include_once INCLUDE_PATH ."/footer.php"; ?>


    </div><!--containerfluid end-->


    <?php include_once INCLUDE_PATH ."/fscript.php"; ?>
</body>


</html>
