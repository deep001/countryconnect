<?php
include_once __DIR__ ."/autoload/define.php";
include_once CLASS_PATH . '/class.session.php';
include_once CLASS_PATH .'/class.database.php';
include_once CONFIG_PATH .'/config.php';
include_once CLASS_PATH . '/class.signin.php';
include_once CLASS_PATH . '/class.csrf.php';
//include_once CLASS_PATH . '/class.ipcountry.php';
include_once CLASS_PATH . '/class.headers.php';
$sessionObj = new Session();
if (!isset($sessionObj)) {
    Headers::redirect("/404");
}
$sessionObj->startSession();
Session::checksession();




?>
<!doctype html>
<html lang="en">



<head>
<title>Admin.ATRIARK.TEST</title>
<?php include_once INCLUDE_PATH ."/head.php"; ?>


</head>
<body class="theme-orange">

<!-- Page Loader -->
<?php include_once INCLUDE_PATH ."/loader.php"; ?>




<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">
<!-- header goes here -->
   <?php include_once INCLUDE_PATH ."/header.php"; ?>
<!-- left bar goes here-->
   <?php include_once INCLUDE_PATH ."/mainmenu.php"; ?>

   <div id="main-content">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Admin Dashboard</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="#">Turnover</a></li>
                       
                        
                        </ol>
                    </nav>
                </div>            
                
            </div>
        </div>

        <div class="container-fluid">
            
            <div class="row clearfix">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing8">
                            <thead>
                                <tr> 
                                    <th>Currency</th>
                                    <th>Total deposits</th>
                                    <th>Total withdrawals</th>
                                    <th>Trade fees</th>
                                    <th>Deposits/Withdrawals</th>
                                    <th>Order matching fees</th>
                                    <th>Affiliate fees</th>
                                    <th>Profiles</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="w60">
                                        BTC
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>+62.18404185</td>
                                    <td>+0.00</td>
                                    <td>+0.00</td>
                                    <td>-0.00</td>
                                    <td>62.18404185 BTC</td>
                                </tr>
                                <tr>
                                    <td class="w60">
                                        ETH
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>+1,518.43979311</td>
                                    <td>+0.00</td>
                                    <td>+0.00</td>
                                    <td>-0.00</td>
                                    <td>1,518.43979311 ETH</td>
                                </tr>
                                <tr>
                                    <td class="w60">
                                        USD
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>+62.18404185</td>
                                    <td>+0.00</td>
                                    <td>+0.00</td>
                                    <td>-0.00</td>
                                    <td>62.18404185 BTC</td>
                                </tr>
                                <tr>
                                    <td class="w60">
                                        EUR
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>+62.18404185</td>
                                    <td>+0.00</td>
                                    <td>+0.00</td>
                                    <td>-0.00</td>
                                    <td>62.18404185 BTC</td>
                                </tr>
                                <tr>
                                    <td class="w60">
                                        BCH
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>+62.18404185</td>
                                    <td>+0.00</td>
                                    <td>+0.00</td>
                                    <td>-0.00</td>
                                    <td>62.18404185 BTC</td>
                                </tr>
                                                           </tbody>
                        </table>
                    </div>
                </div>
                <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h1>Current situation</h1>
                    <nav aria-label="breadcrumb">
                       
                    </nav>
                </div>            
                
            </div>
        </div>          
                
            </div>
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table header-border table-hover table-custom spacing5">
                            <thead>
                                <tr>
                                    <th>Currency</th>
                                    <th>Balance</th>
                                    <th>Pending deposits</th>
                                    <th>pending withdrawals</th>
                                    <th>Funds in orders</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="w60">BTC</th>
                                    <td>1,108,743,040.95714879</td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>2.00
                                    </td>
                                    <td>288,164.73059707
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w60">ETH</th>
                                    <td>1,108,743,040.95714879</td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>2.00
                                    </td>
                                    <td>288,164.73059707
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w60">USD</th>
                                    <td>1,108,743,040.95714879</td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>2.00
                                    </td>
                                    <td>288,164.73059707
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w60">EUR</th>
                                    <td>1,108,743,040.95714879</td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>2.00
                                    </td>
                                    <td>288,164.73059707
                                    </td>
                                </tr>
                                <tr>
                                    <th class="w60">BCH</th>
                                    <td>1,108,743,040.95714879</td>
                                    <td>
                                        0.00
                                    </td>
                                    <td>2.00
                                    </td>
                                    <td>288,164.73059707
                                    </td>
                                </tr>
                             
                            
                           
                             
                            </tbody>
                        </table>
                    </div>
                </div>


               
          
            
       
            </div>

        </div>
    </div>

<?php include_once INCLUDE_PATH ."/footer.php"; ?>
</body>


</html>
