<?php

class Config
{

    const env_production = false;

    public static function App()
    {

        return (object) [
            'title' => 'Countrylink',
            'description' => 'Countrylink',
            'currency' => 'USD',
            'timezone' => 'UTC',
            'path_url' => (self::env_production === true) ? 'http://countrylink.in/' : 'http://countrylinklocal.in',
            'origin' => (self::env_production === true) ? 'http://countrylink.in/' : 'http://countrylinklocal.in',
            'path_url_text' => (self::env_production === true) ? 'http://countrylink.in/' : 'countrylinklocal.in',
            'alt_title' => 'Countrylink',
            'app_name' => 'Countrylink',
            'copyright' => 'Countrylink',
            'secure' => (self::env_production === true) ? true : false,

        ];
    }

    public static function AssetsUrl()
    {
        return (object) [
            'favicon' => "",
            'logo' => ASSET_PATH . '/images/newlogo.png',
            'dark_logo' => ASSET_PATH . '/img/logo-light.png',
            'fb_icon' => ASSET_PATH . '/img/fb.png',
            'twitter_icon' => ASSET_PATH . '/img/twitter.png',
            'instagram_icon' => '',
            'adminavatar' => ASSET_PATH . '/img/user.png',
           

        ];
    } 
    public static function GoogleAuth()
    {
        if (self::env_production === true) {
            return (object) [
                'issuer_name' => 'Atriark,co,uk',
                'code_length' => 6,
            ];
        } else {
            return (object) [
                'issuer_name' => 'Atriark,co,uk',
                'code_length' => 6,
            ];
        }

    }
    public static function GoogleRecaptcha()
    {
        if (self::env_production === true) {
            return (object) [
                'google_captcha_key' => '6LdhZZEUAAAAADyhjUCKg3c-4moncff8iTAuVKKG',
                'google_captcha_secret' => '6LdhZZEUAAAAANX4P5w5219fzMRroGd3TGZ01jE8',
            ];
        } else {
            return (object) [
                'google_captcha_key' => '6LdhZZEUAAAAADyhjUCKg3c-4moncff8iTAuVKKG',
                'google_captcha_secret' => '6LdhZZEUAAAAANX4P5w5219fzMRroGd3TGZ01jE8',
            ];
        }

    }

    public static function SocialLinks()
    {
        return (object) [
            'fb' => 'https://www.facebook.com/bitselly/',
            'twitter' => 'https://twitter.com/bitselly',
            'telegram' => 'https://t.me/bitselly',
            'medium' => 'https://medium.com/@bitselly',
            'youtube' => 'https://www.youtube.com/channel/UCoFMhOcr3FK8rY-eYhnmEew',
        ];
    }

    public static function Debug()
    {
        return (object) [
            'error_reporting' => 0,
        ];
    }

    public static function db()
    {
        if (self::env_production === true) {
            return (object) [
                'default' => (object) [
                    'db_name' => 'countryl_count',
                    'db_user' => 'countryl_coun',
                    'db_password' => 'yi)m_U0Vjl,f',
                    'db_host' => 'localhost',
                    'db_port' => '3306',
                ],
            ];
        } else {
            return (object) [
                'default' => (object) [
                    'db_name' => 'countryl_count',
                    'db_user' => 'root',
                    'db_password' => 'password',
                    'db_host' => '127.0.0.1',
                    'db_port' => '3306',
                ],
            ];
        }
    }

    public static function Sessions()
    {
        return (object) [
            'session_name' => 'SESSID_EX_ADMIN_ATRK',
            'max_session_idle_time' => 14400,
            'cookie_secure' => (self::env_production === true) ? true : false,
            'settings' => (object) [
                'auto_start' => 0,
                'use_cookies' => 1,
                'use_only_cookies' => 1,
                'use_trans_sid' => 0,
                'use_strict_mode' => 1,
                'cookie_httponly' => 1,
                'cookie_domain' => '',
                'cookie_path' => '/',
                'cookie_lifetime' => 28800,
                'cache_limiter' => 'nocache',
                'gc_maxlifetime' => 28800,
                'gc_probability' => 1,
                'gc_divisor' => 100,
                'hash_function' => 1,
                'hash_bits_per_character' => 4,
            ],
        ];
    }

  /*  public static function Email()
    {
        if (self::env_production === true) {
            return (object) [
                'default' => (object) [
                    'host' => 'mail.demo.atriark.co.uk',
                    'port' => 587,
                    'from' => array(
                        'do-no-reply@demo.atriark.co.uk' => 'Atriark',
                    ),
                    'username' => 'noreply@bitselly.com',
                    'password' => '1r{jT)?hk8kh',
                ],
                'support_email' => 'do-not-reply@demo.atriark.co.uk',
                'withdrawal_email' => 'withdrawal@bitselly.com',
            ];
        } else {
            return (object) [
                'default' => (object) [
                    'host' => 'smtp.mailtrap.io',
                    'port' => 2525,
                    'from' => array(
                        'from@atriark.co.uk' => 'Atriark',
                    ),
                    'username' => '585c97fdc5030c',
                    'password' => '22a66d8356171c',
                ],
                'support_email' => 'support@atriark.co.uk',
                'withdrawal_email' => 'withdrawal@bitselly.com',
            ];
        }
    }*/

    public static function SocialUsersLinks()
    {
        return (object) [
            'fb' => 'https://www.facebook.com/',
            'twitter' => 'https://twitter.com/',
            'instagram' => 'https://www.instagram.com/',
            'linkedin' => 'https://www.instagram.com/',
        ];
    }
    public static function Encryption()
    {
        return (object) [
            'salt' => '563f7e758ce7c91b756c659ac634cdfec1108e760c3347181aa6024b6e16c9c0',
            'coin_keys_path' => APP_PATH . '/keys',
        ];
    }
    static public function CronPath() {
        if (self::env_production === TRUE) {
            return (object) [
                        'path' => '/home/useratriarkco/www'
            ];
        } else {
            return (object) [
                        'path' => '/Users/mohitbatra/www/exchange.atriark.test' . APP_PATH . '/cron'
            ];
        }
    }

    public static function checkstatus()
    {
        return (object) [
            'loginfailed' => 'invalid credentials ! Try again',
            'logout' => 'Logout Successfully done',
            'checkstatus' => 'You need to login first!',
            'token' => 'invalid token !',
          

        ];
    }

}
date_default_timezone_set(Config::App()->timezone);
