<?php

include_once __DIR__ . "/../../autoload/define.php";
include_once CLASS_PATH . '/class.session.php';
include_once CONFIG_PATH . "/config.php";
include_once CLASS_PATH .'/class.database.php';
include_once CLASS_PATH . '/class.headers.php';
include_once CLASS_PATH .'/class.errorlist.php';
include_once CLASS_PATH .'/class.validation.php';

include_once CLASS_PATH . '/class.googlelib.php';
//include_once 'class.headers.php';

//require_once 'class.googlelib.php';

//require_once 'class.emails.php';
//require_once 'class.emailtemplates.php';
//require_once 'class.ipcountry.php';
//require_once 'class.browser.php';
$sessionObj = new Session();
if (!isset($sessionObj)) {
    Headers::redirect("/404");
}
$sessionObj->startSession();

class Signin {

    const LOGIN_STATUS_PENDING = "PENDING";
    const LOGIN_STATUS_SUCCESS = "SUCCESS";
    const LOGIN_STATUS_FAILED = "FAILED";

    private $db;

    function __construct() {
        $this->db = new Database();
    }

    private function insertSignInHistory($uuid, $ipAddress, $ip2Dec, $countryCode, $countryName, $googleAuthStatus, $googleAuthCode, $status) {
        if (isset($uuid) && isset($ipAddress) && isset($ip2Dec) && isset($countryCode) && isset($countryName) && isset($googleAuthStatus) && isset($googleAuthCode) && isset($status)) {
            $browserObj = new Browser();
            $this->db->query("insert into login_history set uuid=:UUID, ip_address=:IP, ip2dec=:IP2DEC, country_code=:COUNTRYCODE, country_name=:COUNTRYNAME, google_auth_status=:GAUTHSTATUS, google_auth_code=:GAUTHCODE,device=:DEVICE,browser=:BROWSER,platform=:PLATFORM,entry_time=:ETIME, status=:STATUS");
            $insert = $this->db->execute(array(
                ":UUID" => $uuid,
                ":IP" => $ipAddress,
                ":IP2DEC" => $ip2Dec,
                ":COUNTRYCODE" => $countryCode,
                ":COUNTRYNAME" => $countryName,
                ":GAUTHSTATUS" => $googleAuthStatus,
                ":GAUTHCODE" => $googleAuthCode,
                ":DEVICE" => ($browserObj->getDevice() !== NULL) ? $browserObj->getDevice() : "",
                ":BROWSER" => ($browserObj->getBrowser() !== NULL) ? $browserObj->getBrowser() : "N/A",
                ":PLATFORM" => ($browserObj->getPlatform() !== NULL) ? $browserObj->getPlatform() : "N/A",
                ":ETIME" => date('Y-m-d h:i:s'),
                ":STATUS" => $status
            ));
            if ($insert) {
                $lastInsertId = $this->db->lastInsertId();
                return $lastInsertId;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function updateSigninHistoryStatus($loginHistoryId, $status) {
        if (isset($loginHistoryId) && isset($status)) {
            $loginHistoryId = (int) $loginHistoryId;
            $this->db->query("update login_history set status=:STATUS where id=:ID");
            $this->db->execute(array(
                ":STATUS" => $status,
                ":ID" => $loginHistoryId
            ));
            if ($this->db->rowCount() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    private function verifyEmailnPassword($userEmail, $userPassword) {
        if (isset($userEmail) && isset($userPassword)) {
          //  $userObj = new Users();
         //  $userMeta = $userObj->getUserMetaByEmail($userEmail);
           $userMeta =   $this->getuserrecord($userEmail);
            if ($userMeta) {
             $passwordHash = $userMeta->password;
          
           
               if (password_verify($userPassword, $passwordHash)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

   public function getuserrecord($email){
    if (isset($email) && Validation::validateEmail($email)) {

       $this->db->query("select * from admin_users where email=:EMAIL");
        $this->db->execute(array(
            ":EMAIL" => $email
        ));
        if ($this->db->rowCount() > 0) {
            $rows = $this->db->fetch();
            return $rows;
        } else {
            return FALSE;
        }
    } else {
        return FALSE;
    }
}

    function sendLoginEmail($loginHistoryId) {
        if (isset($loginHistoryId)) {
            $this->db->query("select * from login_history where id=:ID");
            $this->db->execute(array(
                ":ID" => (int) $loginHistoryId
            ));
            if ($this->db->rowCount() > 0) {
                $row = $this->db->fetch();
                $ipAddress = $row->ip_address;
                $country = $row->country_name;
                $uuid = $row->uuid;
                $userObj = new Users();
                $userMeta = $userObj->getUserMeta($uuid);
                $emailObj = new Emails();
                $emailObj->sendEmail($userMeta->email, EmailTemplates::getSuccessLoginSubject(Config::App()->title, date("Y-m-d H:i:s")), EmailTemplates::getSuccessLoginTemplate(Config::App()->title, $userMeta->email, $ipAddress, $country), Config::Email()->default->username, Config::Email()->default->password);
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    function init_signin($userEmail, $userPassword, $gRecaptchaResponse) {
        if (isset($userEmail) && isset($userPassword) && isset($gRecaptchaResponse)) {
            $userEmail = trim(strtolower(filter_var($userEmail, FILTER_SANITIZE_EMAIL)));
            $userPassword = trim(filter_var($userPassword, FILTER_SANITIZE_STRING));
            $gRecaptchaResponse = trim(filter_var($gRecaptchaResponse, FILTER_SANITIZE_STRING));

            $googleObj = new Googlelib();
            if (!$googleObj->gRecaptchaValidate($gRecaptchaResponse)) {
                return Errorlist::errorResponse(FALSE, "invalid googgRecaptchaValidate recaptcha, Please try again");
            } else if (!Validation::validateEmail($userEmail)) {

                return Errorlist::errorResponse(FALSE, "invalid emaigRecaptchaValidateaddress");
            } else if (!Validation::validatePasswordStrength($userPagRecaptchaValidateword)) {

                return Errorlist::errorResponse(FALSE, "invalid password strength, it can contain atleast 1 upper and 1 lower alphabet, 1 number, 1 special character @!#&%*");
            } else if (!$this->verifyEmailnPassword($userEmail, $userPassword)) {

                return Errorlist::errorResponse(FALSE, "incorrect email or password");
            } else {
                $userObj = new Users();
                $uuid = $userObj->getUUIDbyEmail($userEmail);
                if (!$uuid) {
                    return (object) $res = array(
                        "status" => FALSE,
                        "msg" => "incorrect email or password!!"
                    );
                } else {
                    $ipCountryObj = new IpCountry();
                    $ipAddress = $GLOBALS['ip'];
                    $getCountryMetaByIp = $ipCountryObj->getCountryMetaByIP($ipAddress);

                    $google2FA = $userObj->checkGoogle2FA($userEmail);

                    if ($google2FA) {
                        //if user has google authenticater active
                        if ($getCountryMetaByIp) {
                            $insertLoginHistory = $this->insertSignInHistory($uuid, $ipAddress, ip2long($ipAddress), $getCountryMetaByIp->country_code, $getCountryMetaByIp->country_name, Users::USER_GOOG_AUTH_VERIFIED_YES, 'NA', Signin::LOGIN_STATUS_PENDING);
                        } else {
                            $insertLoginHistory = $this->insertSignInHistory($uuid, 'NA', 'NA', 'NA', 'NA', Users::USER_GOOG_AUTH_VERIFIED_YES, 'NA', Signin::LOGIN_STATUS_PENDING);
                        }

                        return (object) $res = array(
                            "status" => TRUE,
                            "msg" => "Waiting for Google Auth Code",
                            "uuid" => $uuid,
                            "gAuth" => TRUE,
                            "loginHistoryId" => $insertLoginHistory
                        );
                    } else {
                        //if user has'not google authenticater active
                        if ($getCountryMetaByIp) {
                            $insertLoginHistory = $this->insertSignInHistory($uuid, $ipAddress, ip2long($ipAddress), $getCountryMetaByIp->country_code, $getCountryMetaByIp->country_name, Users::USER_GOOG_AUTH_VERIFIED_NO, 'NA', Signin::LOGIN_STATUS_SUCCESS);
                        } else {
                            $insertLoginHistory = $this->insertSignInHistory($uuid, 'NA', 'NA', 'NA', 'NA', Users::USER_GOOG_AUTH_VERIFIED_NO, 'NA', Signin::LOGIN_STATUS_SUCCESS);
                        }

                        return (object) $res = array(
                            "status" => TRUE,
                            "msg" => "Login Successful",
                            "uuid" => $uuid,
                            "gAuth" => False,
                            "loginHistoryId" => $insertLoginHistory
                        );
                    }
                }
            }
        } else {
            return Errorlist::errorResponse(FALSE, "incorrect email or password");
        }
    }
    public function log($post)
    { 
       $useremail = $post['u_email'];
      $useremail = strtolower(filter_input(INPUT_POST, 'u_email', FILTER_SANITIZE_EMAIL));
     
       $password = $post['u_password'];
      $password = strtolower(filter_input(INPUT_POST, 'u_password', FILTER_SANITIZE_STRING));
      
     $gRecaptchaResponse = filter_input(INPUT_POST, 'g-recaptcha-response', FILTER_SANITIZE_STRING);
      
      $csrfToken = $post['csrf-token'];
      $csrfToken = filter_input(INPUT_POST, 'csrf-token', FILTER_SANITIZE_STRING);

      $googleObj = new Googlelib();
      if (!$googleObj->gRecaptchaValidate($gRecaptchaResponse)) {
        return Errorlist::errorResponse(FALSE, "invalid google recaptcha, Please try again");
      }
      else
      {
    if($_SESSION['csrf'] == $csrfToken)
    {
        $this->db->query("select * from admin_users where email=:EMAIL AND password=:PASSWORD");
   $exe =  $this->db->execute(array(
        ":EMAIL" => $useremail,
         ":PASSWORD" => $password
    ));
     if ($this->db->rowCount() > 0) {
        $row = $this->db->fetch();

       $_SESSION['email'] =  $row->email;
        $_SESSION['uid'] =  $row->id;
       Headers::redirect('/dashboard.php'); 
     
     } else {
     Headers::redirect('/index.php?loginfailed=1'); 
     
    } 
    
    }
    else
    {
        Headers::redirect('/index.php?token=1'); 
    }
    }
    }

}
//$a=new Signin();
//$a->log("mohit2979@gmail.com","Mhit@2979");