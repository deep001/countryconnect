<?php
include_once __DIR__ . "/../config/config.php";

class Googlelib
{

    public function gRecaptchaValidate($gRecaptchaResponse)
    {
        if (isset($gRecaptchaResponse)) {
            $response = $gRecaptchaResponse;
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = http_build_query(array(
                'secret' => Config::GoogleRecaptcha()->google_captcha_secret,
                'response' => $response,
            ));

            $options = array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Accept-language: en\r\n" .
                    "Content-type: application/x-www-form-urlencoded\r\n",
                    'content' => $data,
                ),
            );
            $context = stream_context_create($options);
            $verify = file_get_contents($url, false, $context);
            $captcha_success = json_decode($verify);
            if ($captcha_success->success == true) {
                return true;
            } else if ($captcha_success->success == false) {
                return false;
            }
        } else {
            return false;
        }
    }

}
