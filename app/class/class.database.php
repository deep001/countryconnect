<?php

include_once __DIR__ . "/../../autoload/define.php";
include_once CONFIG_PATH . "/config.php";
use PDO;
class Database {

    private $host;
    private $user;
    private $pass;
    private $dbname;
    private $port;
    private $dbh;
    private $error;
    private $stmt;

    public function __construct() {
        
      /*  $this->host = Config::db()->default->db_host;
        $this->port = Config::db()->default->db_port;
        $this->dbname = Config::db()->default->db_name;
        $this->user = Config::db()->default->db_user;
        $this->pass = Config::db()->default->db_password;
      */  
        
     /*    'db_name' => 'countryl_count',
                    'db_user' => 'countryl_coun',
                    'db_password' => 'yi)m_U0Vjl,f',
                    'db_host' => 'localhost',
                    'db_port' => '3306', */
$this->host = 'localhost';
        $this->port = '3306';
        $this->dbname = 'countryl_count';
        $this->user = 'root';
        $this->pass = 'password';
        // Set DSN
        $dsn = 'mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->dbname;
       
        // Set options
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );
        // Create a new PDO instanace
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
          
        }
        // Catch any errors
        catch (PDOException $e) {
            $this->error = $e->getMessage();
        }

    }

    public function query($query) {
      
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute($parms = NULL) {
        if (isset($parms)) {
            return $this->stmt->execute($parms);
        } else {
            return $this->stmt->execute();
        }
    }

    public function fetchAll() {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function fetch() {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    public function rowCount() {
        return $this->stmt->rowCount();
    }

    public function lastInsertId() {
        return $this->dbh->lastInsertId();
    }

    public function beginTransaction() {
        return $this->dbh->beginTransaction();
    }

    public function InTransaction() {
        return $this->dbh->inTransaction();
    }

    public function endTransaction() {
        return $this->dbh->commit();
    }

    public function setAutoCommitFalse() {
        return $this->dbh->setAttribute(PDO::ATTR_AUTOCOMMIT, 0);
    }

    public function setAutoCommitTrue() {
        return $this->dbh->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
    }

    public function cancelTransaction() {
        return $this->dbh->rollBack();
    }

    public function debugDumpParams() {
        return $this->stmt->debugDumpParams();
    }

    public function endConnection() {
        $this->dbh = NULL;
        return TRUE;
    }

}


