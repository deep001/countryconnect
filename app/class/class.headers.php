<?php

class Headers {

    static public function setHTTPHeader($httpCode) {
        if ($httpCode === 200) {
            header('HTTP/1.1 200 OK');
        } else if ($httpCode === 400) {
            header('HTTP/1.1 400 Bad request');
        } else if ($httpCode === 404) {
            header('HTTP/1.1 404 not found');
        } else if ($httpCode === 403) {
            header('HTTP/1.1 403 Forbidden');
        } else if ($httpCode === 301) {
            header("HTTP/1.1 301 Moved Permanently");
        } else if ($httpCode === 302) {
            header("HTTP/1.1 302");
        }
    }

    static public function setJSONHeader() {
        header("CONTENT-TYPE: application/json; charset=UTF-8");
    }

    static public function redirect($path, $replace = false, $httpCode = NULL) {
       
        if (isset($path) && is_string($path)) {
            if ($replace === TRUE && isset($httpCode) && is_int($httpCode)) {
                header("location: " . trim($path), $replace, $httpCode);
            } else {
                header("location: " . trim($path));
            }

            exit();
        }
    }

}
