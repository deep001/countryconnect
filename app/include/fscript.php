
    <script src="<?php echo ASSET_PATH;?>/js/jquery.js"></script>
    <script src="<?php echo ASSET_PATH;?>/js/bootstrap.min.js"></script>
    <script src="<?php echo ASSET_PATH;?>/js/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {

            // Slideshow 4
            $("#slider1").responsiveSlides({
                auto: true,
                pager: false,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <script type="text/javascript">
        $(function () {
            $(".dropdown").hover(
                    function () {
                        $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                        $(this).toggleClass('open');
                        $('b', this).toggleClass("caret caret-up");
                    },
                    function () {
                        $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                        $(this).toggleClass('open');
                        $('b', this).toggleClass("caret caret-up");
                    });
        });
    </script>
    <script type="text/javascript">
        // When the DOM is ready, run this function
        $(document).ready(function () {
            //Set the carousel options
            $('#quote-carousel').carousel({
                pause: true,
                interval: 4000,
            });
        });
    </script>