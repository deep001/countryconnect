<div class="footer">
        <div class="container">
            <div class="col-md-3 col-sm-12">
                <div class="footercontent">
                    <small><i class="fa fa-user"></i>&nbsp; About Us </small>
                    <p>We are a world-class Internet Service Provider (ISP) that caters to both home and business segments.</p>
                </div>
                <!--footercontent end-->
            </div>
            <!--col3 end-->

            <div class="col-md-3 col-sm-12">
                <div class="footercontent">
                    <!--<small><i class="fa fa-phone"></i>&nbsp; Call Us Today : </small>
                    <strong>(+91) 9555112277, 9015112277</strong>-->
                </div>
                <!--footercontent end-->
            </div>
            <!--col3 end-->

            <div class="col-md-3 col-sm-12">
                <div class="footercontent">
                    <small><i class="fa fa-envelope"></i>&nbsp; Email Us : </small>
                    <strong><a href="mailto:info@zeonet.co.in">info@countrylink.in</a></strong>
                </div>
                <!--footercontent end-->
            </div>
            <!--col3 end-->

            <div class="col-md-3 col-sm-12">
                <div class="footercontent">
                    <small><i class="fa fa-hand-o-right"></i>&nbsp; Follow Us On : </small>
                    <strong>
                        <a href="http://facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="http://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="http://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a>
                    </strong>
                </div>
                <!--footercontent end-->
            </div>
            <!--col3 end-->
        </div>
        <!--container end-->
    </div>
    <!--footer end-->
    <div class="clearfix"></div>

    <div class="footerend">
        <p>
            &copy; Copyright 2019. All Right Reserved.
Managed By : <a href="http://countrylink.in/" target="_blank" style="color: #fff;">Develop by</a><a href="http://www.seowebmantra.com" target="_blank">www.seowebmantra.com</a>
        </p>
    </div>