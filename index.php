<?php
include_once __DIR__ ."/autoload/define.php";
include_once CONFIG_PATH .'/config.php';
include_once CLASS_PATH .'/class.database.php';
include_once CLASS_PATH . '/class.headers.php';
include_once CLASS_PATH . '/class.seller.php'; 


$getseller = new Seller;
$res = $getseller->getSellerRecord();
//echo $res;die;
//print_r($res);die;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<?php include_once INCLUDE_PATH ."/head.php"; ?>
</head>

<body>

<?php  include_once INCLUDE_PATH ."/header.php"; ?>
    

    <!--topheader end-->

 
    <div id="headerwrap" name="home">
        <header class="clearfix">
	  		 		<ul class="rslides" id="slider1">
            <li><img src="<?php echo ASSET_PATH;?>/images/slide1.jpg" alt="Broadband internet connection Haryana" /></li>
            <li><img src="<?php echo ASSET_PATH;?>/images/slide2.jpg" alt="Zeonet Internet Network Services" /></li>
            <li><img src="<?php echo ASSET_PATH;?>/images/slide3.jpg" alt="Internet Service Provider" /></li>
            <li><img src="<?php echo ASSET_PATH;?>/images/slide4.jpg" alt="High speed internet in Delhi" /></li>
          </ul>
	  		</header>
    </div>


    <div class="container">
 <div class="table-responsive">
					<table class="table">
					  <thead class="thead-light">
						<tr>
                          <th scope="col" width="5%">Sr.&nbsp;No.</th>
						  <th scope="col" width="22%">Email</th>
                          <th scope="col" width="22%">Firstname</th>
						  <th scope="col" width="22%">Lastname</th>
                          <th scope="col" width="22%">Mobilenumber</th>
                          <th scope="col" width="22%" colspan="2">Action</th>
						</tr>
					  </thead> 
					<?php
					 $i=1;
		 foreach($res as $roww)
						//while($row = $res->fetch_assoc())
		 {
				echo '<tr>
						 <td scope="col">#<a href="worker_profile.php?id='.$row->id.'">'.$roww->id.'</a></td>
						  <td scope="col">'.$roww->email.'</td>
                          <td scope="col">'.$roww->Firstname.'</td>
						  <td scope="col">'.$roww->Lastname.'</td>
                          <td scope="col">'.$roww->Mobilenumber.'</td>
						  <td scope="col"><a href="edit_profile.php?id='.$roww->id.'">Edit</td>
						  <td scope="col"><a href="delete.php?id='.$roww->id.'">Delete</td>
					</tr>';
				$i++;
		 } 
					?>
					</table>
				</div>
        <div class="benefitbg">
            <h2>Customer Benefits</h2>
            <p>We delight customers completely with distinctive features of our world-class service.</p>
            <div class="col-md-4 col-sm-12">
                <div class="benefit">
                    <img src="images/ben1.jpg" alt="Benefit" />
                    <h3>SUPER FAST</h3>
                    <p>We delight our customers with an exceptional internet speed. We make our best efforts to give you uninterrupted service always.</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
                <!--benefit end-->
            </div>
            <!--col4 end-->

            <div class="col-md-4 col-sm-12">
                <div class="benefit">
                    <img src="images/ben2.jpg" alt="Benefit" />
                    <h3>UNLIMITED ENTERTAINMENT</h3>
                    <p>With hi-speed internet access, download your favorite movies, songs and games swiftly. Enjoy HD videos on Youtube, Hotstar and Torbox without buffering.</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
                <!--benefit end-->
            </div>
            <!--col4 end-->

            <div class="col-md-4 col-sm-12">
                <div class="benefit">
                    <img src="images/ben3.jpg" alt="Benefit" />
                    <h3>FAIR USAGE POLICY</h3>
                    <p>We stay true to our customers and thus the data limit of your package never drops down throughout the service. Satisfactory service at honest prices</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
                <!--benefit end-->
            </div>
            <!--col4 end-->

      <!--      <div class="col-md-4 col-sm-12">
                <div class="benefit">
                    <img src="images/ben4.jpg" alt="Benefit" />
                    <h3>CUSTOMER SUPPORT</h3>
                    <p>We have a dedicated and proactive team of experts to hear your queries and resolve problems concerned with our service in shortest time frame.</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
              
            </div>
           


            <div class="col-md-4 col-sm-12">
                <div class="benefit">
                    <img src="images/ben5.jpg" alt="Benefit" />
                    <h3>EASE OF USE</h3>
                    <p>With our auto-renewal policy, we make sure our customers have a convenient experience with us. Billing cycle renews automatically.</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
               
            </div>
           


            <div class="col-md-4 col-sm-12">
                <div class="benefit">
                    <img src="images/ben6.jpg" alt="Benefit" />
                    <h3>GENUINE PRICES</h3>
                    <p>There are no hidden charges. You will pay the exact amount as provided on the website for a particular package..</p>
                    <a href="#" class="readmore">Read More</a>
                </div>
              
            </div>
-->

        </div>
        <!--benefit end-->
    </div>
    <!--container end-->
    <div class="clearfix"></div>

    <div class="planbg">
        <div class="container">
            <h2>Our Specialities</h2>
            <div class="col-md-6 col-sm-12">
                <div class="special">
                    <img src="images/special.jpg" alt="Specialities" title="Specialities" />
                </div>
                <!--special end-->
            </div>
            <!--col6 end-->

            <div class="col-md-6 col-sm-12">
                <p>
                    In various way differentiate . We work according to a four-step system. A diligent and meticulous analysis of the client's problem/project, animates the development process, which 
    then leads on to a comprehensive and effective solution.
                </p>
                <ul class="sitelist">
                    <li>A Total Focus on Delivering</li>
                    <li>Speed &amp; Reliability</li>
                    <li>Highly Advanced Infrastructure</li>
                    <li>Underground OFC Backbone</li>
                    <li>100% Non Blocking Throughout</li>
                    <li>Customized SLA</li>
                    <li>Guaranteed bandwidth for critical business usage, (Symmetric 1:1 bandwidth only)</li>
                    <li>Reliable internet connection (24 x 7) with redundancy on domestic link</li>
                    <li>24 hour network / security monitoring &amp; technical helpdesk</li>
                    <li>Dedicated support team for Corporate Client</li>
                </ul>
            </div>
            <!--col6 end-->
        </div>
    </div>
    <!--planbg end-->
    <div class="clearfix"></div>


        <div class="testimonial">
        <div class="container">
            <div class="row">
                <div class='col-md-offset-2 col-md-8 text-center'>
                    <h2>Client Says</h2>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-12'>
                    <div class="carousel slide" data-ride="carousel" id="quote-carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#quote-carousel" data-slide-to="1"></li>
                            <li data-target="#quote-carousel" data-slide-to="2"></li>
                        </ol>


                        <div class="carousel-inner">

                            <div class="item active">
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-3 text-center">
                                            <img class="img-circle" src="http://www.reactiongifs.com/r/overbite.gif" style="width: 100px; height: 100px;">
                                        </div>
                                        <div class="col-sm-9">
                                            <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
                                            <small>Someone famous</small>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="item">
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-3 text-center">
                                            <img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/mijustin/128.jpg" style="width: 100px; height: 100px;">
                                        </div>
                                        <div class="col-sm-9">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
                                            <small>Someone famous</small>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="item">
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-3 text-center">
                                            <img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/keizgoesboom/128.jpg" style="width: 100px; height: 100px;">
                                        </div>
                                        <div class="col-sm-9">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.</p>
                                            <small>Someone famous</small>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                        </div>

                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="clearfix"></div>


    <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3444.856141546528!2d78.05103541455831!3d30.29815678179063!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390929087a1f3545%3A0xd3c934e805d47a14!2sNishima+Complex%2C+Nehru+Colony+Rd%2C+Nehru+Colony%2C+Drone+Puri%2C+Dharampur%2C+Dehradun%2C+Uttarakhand+248001!5e0!3m2!1sen!2sin!4v1549614640067" width="100%" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!--map end-->
    <div class="clearfix"></div>


    <div class="getintouch">
        <h1>Get In Touch</h1>
        <div class="container">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <input type="text" placeholder="Your Name" class="form-control getinput" />
                </div>

                <div class="form-group">
                    <input type="text" placeholder="Your Email" class="form-control getinput" />
                </div>

                <div class="form-group">
                    <input type="text" placeholder="Your Phone" class="form-control getinput" />
                </div>
            </div>
            <!--col6 end-->

            <div class="col-md-6 col-sm-12">
                <textarea name="" cols="" rows="" class="form-control getarea" placeholder="Your Message"></textarea>
            </div>
            <!--col6 end-->

            <div class="col-md-12 text-center">
                <a href="#">
                    <input type="submit" value="SENT MESSAGE" class="btn btn-primary" /></a>
            </div>
        </div>
        <!--container end-->
    </div>
    <!--getintouch end-->
    <div class="clearfix"></div>


    <?php  include_once INCLUDE_PATH ."/footer.php"; ?>
  


    </div>


    <?php  include_once INCLUDE_PATH ."/fscript.php"; ?>
</body>

</html>
