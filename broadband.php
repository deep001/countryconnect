﻿<?php
include_once __DIR__ ."/autoload/define.php";
include_once CONFIG_PATH .'/config.php';
include_once CLASS_PATH . '/class.headers.php';


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>
<?php include_once INCLUDE_PATH ."/head.php"; ?>


    <style>
        .pricing-tables {
            padding: 20px;
        }

            .pricing-tables h1 {
                font-size: 48px;
            }

            .pricing-tables .plan.first {
                border-bottom-left-radius: 4px;
                border-top-left-radius: 4px;
            }

            .pricing-tables .plan.last {
                border-bottom-right-radius: 4px;
                border-top-right-radius: 4px;
            }

            .pricing-tables .plan.recommended {
                border-bottom-left-radius: 4px;
                border-bottom-right-radius: 4px;
            }

                .pricing-tables .plan.recommended .head {
                    margin-bottom: 20px;
                    border-top-left-radius: 4px;
                    border-top-right-radius: 4px;
                }

            .pricing-tables.attached .col-sm-4,
            .pricing-tables.attached .col-md-4,
            .pricing-tables.attached .col-sm-3,
            .pricing-tables.attached .col-md-3 {
                padding-left: 0;
                padding-right: 0;
            }

            .pricing-tables.attached .plan {
                border-radius: 0;
            }

                .pricing-tables.attached .plan .head {
                    border-radius: 0;
                }

                .pricing-tables.attached .plan.recommended {
                    border-radius: 4px;
                }

                    .pricing-tables.attached .plan.recommended .head {
                        border-top-left-radius: 4px;
                        border-top-right-radius: 4px;
                    }

                .pricing-tables.attached .plan.last {
                    border-bottom-right-radius: 4px;
                }

                    .pricing-tables.attached .plan.last .head {
                        border-top-right-radius: 4px;
                    }

                .pricing-tables.attached .plan.first {
                    border-bottom-left-radius: 4px;
                }

                    .pricing-tables.attached .plan.first .head {
                        border-top-left-radius: 4px;
                    }

        .plan {
            box-shadow: 0 2px 2px rgba(10, 10, 10, 0.06);
            min-height: 100px;
            background: #fff;
            border-radius: 4px;
            margin: 20px 0;
            padding-bottom: 25px;
            text-align: center;
        }

            .plan .head {
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
                padding: 12px 16px;
                background: #42939e;
                color: #fff;
            }

                .plan .head h1, .plan .head h2, .plan .head h3 {
                    padding: 0;
                    margin: 0;
                    font-weight: 100;
                }

            .plan .price {
                border-top: 1px solid #eee;
                margin: 0 auto 30px auto;
                width: 80%;
            }

                .plan .price h3 {
                    font-size: 50px;
                    vertical-align: top;
                    line-height: 1;
                }

                    .plan .price h3 span {
                        font-size: 38px;
                        vertical-align: top;
                        position: relative;
                        margin: 6px 0 0 -7px;
                        display: inline-block;
                    }

                .plan .price h4 {
                    color: #aaa;
                    font-size: 14px;
                }

            .plan .btn {
                padding: 10px 30px;
                text-transform: uppercase;
                font-weight: 500;
            }

            .plan ul {
                list-style-type: none;
                padding: 20px;
                margin-top: 2px;
            }

                .plan ul li {
                    line-height: 22px;
                    margin-bottom: 15px;
                    font-size: 14px;
                    font-weight: 400;
                }

                    .plan ul li a {
                        text-decoration: underline;
                        color: #e6e9ed;
                    }

                    .plan ul li:last-child {
                        border-bottom: none;
                    }

                .plan ul strong {
                    font-weight: 700;
                }

            .plan.recommended {
                margin-top: 6px;
                box-shadow: 0 0 22px rgba(10, 10, 10, 0.42);
                position: relative;
                z-index: 99;
                border-radius: 4px;
            }

                .plan.recommended .head {
                    border-top-left-radius: 4px;
                    border-top-right-radius: 4px;
                    background: #171717;
                }

                .plan.recommended .btn {
                    margin-bottom: 10px;
                }



        .plans {
            text-align: center;
            background-color: #fff;
            padding: 2%;
            box-shadow: 0px 0px 2px 1px rgb(202, 198, 198);
            margin-top: 2%;
            margin-left: 4%;
        }

            .plans h4 {
                color: #777777;
            }
    </style>







</head>

<body>


<?php  include_once INCLUDE_PATH ."/header.php"; ?>



    <header class="page-header">

<div class="container-fluid">

<img src="images/broadband.jpg" alt="broadband internet plans Rajasthan" class="img-responsive" />

</div>


</header>


    <div class="planbg">
        <div class="container">
            <h2>BROADBAND</h2>
            <div class="col-md-4 col-sm-12">
                <div class="special">
                    <img src="images/about.jpg" class="img-responsive" />
                </div>
                <!--special end-->
            </div>
            <!--col6 end-->

            <div class="col-md-8 col-sm-12">
                <h4>Broadband on Fibre</h4>
                <br />
                <p>When speed is all what you need, switch to Zeonet Broadband on Fibre. Optical Fibre technology allows instant connectivity via a central point right to your residences, apartments and corporate offices for an exceptional internet speed. The best part about fibre broadband is that it gives an access to high speed connections and has increased carrying capacity unlike twisted pair conductors or coaxial cable. With the enhanced bandwidth and state-of-the-art technology, fibre connectivity comes at amazing affordable prices. </p>
                <p>
                    Join the Zeonet family today to redefine your internet experience with the lightening fast surfing speed. 
    
                </p>
            </div>
            <!--col6 end-->







        </div>
    </div>
    <!--planbg end-->
    <div class="clearfix"></div>





    <div class="planbg">
        <div class="container">
            <h2>Wireless Broadbands</h2>
            <div class="col-md-8 col-sm-12">
                <h4>Zeonet Wireless </h4>
                <br />
                <p>Unlimited, fast and affordable internet can be yours without any fuss. Wireless broadband connectivity gives instant and hassle free internet access to your premises at low prices. </p>
                <p>
                    A Zeonet CPE (Customer Premises Equipment) would be provided to you through which wireless signal exudes and reaches to your PC, laptops and smartphones. At present, we are catering many happy customers in various regions of India with our unbeatable broadband plans.
                </p>
                <p></p>
            </div>
            <!--col6 end-->

            <div class="col-md-4 col-sm-12">
                <div class="special">
                    <img src="images/special.jpg" class="img-responsive" />
                </div>
                <!--special end-->

            </div>
            <!--col6 end-->







        </div>
    </div>
    <!--planbg end-->
    <div class="clearfix"></div>



    <div class="clearfix"></div>






    <?php  include_once INCLUDE_PATH ."/footer.php"; ?>


    </div><!--containerfluid end-->

    <?php  include_once INCLUDE_PATH ."/fscript.php"; ?>
</body>


</html>
