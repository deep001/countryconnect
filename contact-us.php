<?php
include_once __DIR__ ."/autoload/define.php";
include_once CONFIG_PATH . "/config.php";
include_once CLASS_PATH . '/Contactus.php';





if (isset($_POST['send'])) {
    $validationRes = new stdClass();
    if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['mobile']) && isset($_POST['message'])) {
        $userName = strtolower(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING));
        $userEmail = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
       // $gRecaptchaResponse = filter_input(INPUT_POST, 'g-recaptcha-response', FILTER_SANITIZE_STRING);
        $mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_STRING);
        $message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);

        $contactObj = new Contactus();
        $contactprocess = $contactObj->inquiryMail($userName, $userEmail,$mobile,$message);
        if($contactprocess->status == false)
        {
            $failuremessage = $contactprocess->msg;
        }
        else
        {
            $successmessage = $contactprocess->msg;
        }
}
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<?php include_once INCLUDE_PATH ."/head.php"; ?>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>
</head>

<body>


<?php include_once INCLUDE_PATH ."/header.php"; ?>


<header class="page-header">

<div class="container-fluid">

<img src="images/contactus.jpg" class="img-responsive"  />

</div>


</header>


    <div class="planbg">
        <div class="container">
            <h2>Contact us</h2>

            <div class="col-md-6 col-sm-12">
                <p>
                    <h4><strong>Head Office</strong></h4>
                </p>
                <p>
                    Countrylink Communication Pvt Ltd.<br />
1st floor Nishima Complex opp. Gurudwara Nehru coloney Dehradun - 248001
<!--        <br />
                    <br />
                    <strong>Phone:</strong>  (+91) 9555112277, 9015112277-->
                    <br />
                    <strong>E-mail:</strong> info@countrylink.in
                </p>
                <br />

                <p>
                    <h4><strong>Haridwar Office</strong></h4>
                </p>
                <p>
                Countrylink Communication Pvt Ltd.<br />
Prabhakar Complex, Second Floor,396 Vivek Vihar Ranipur More, Haridwar - 249407

<!--4754 , Shankar Garden Colony Nakodar ,
Dist : Jalandhar , Punjab-->
<!--        <br />
                    <br />
                    <strong>Phone:</strong>  (+91) 9555112277, 9015112277-->
                    <br />
                    <strong>E-mail:</strong> info@countrylink.in
                </p>

            </div>
            <!--col6 end-->


            <div class="col-md-6 col-sm-12">
                <div class="special">
                <?php if(isset($successmessage) && !empty($successmessage)){ 
                       echo '<div style="background-color:blue;padding: 4px 4px 4px 4px;color:#fff;font-weight:bold;">';
                       echo $successmessage;
                       echo '</div>';
                  }
                        if(isset($failuremessage) && !empty($failuremessage)){ 
                       echo '<div style="background-color:red;padding: 4px 4px 4px 4px;color:#fff;font-weight:bold;">';
                       echo $failuremessage;
                       echo '</div>';
                  } ?>
                    <form action="<?php echo $_SERVER['PHP_SELF'];?>" id="contact-form" method="post">
                        <p>
                            <label for="contactName2"></label>
                            <input class="radius" type="text" name="name" id="contactName2" value="" placeholder="Name*" aria-required="true" required="" style="width: 100%;">
                            <span class="clear"></span>
                        </p>
                        <p>
                            <label for="email"></label>
                            <input class="radius" type="email" name="email" id="email" value="" placeholder="Email Address*" required="" style="width: 100%;"  >
                            <span class="clear"></span>
                        </p>
                        <p>
                            <label for="MObile"></label>
                            <input class="radius" name="mobile" type="text" aria-required="true" required="" pattern="[789][0-9]{9}" placeholder="Mobile*" style="width: 100%;">
                            <span class="clear"></span>
                        </p>
                        <p>
                            <label for="commentsText"></label>
                            <textarea class="contactme-text required requiredField radius" name="message" cols="10" rows="10" placeholder="Message" style="height: 120px;" required=""></textarea>
                            <span class="clear"></span>
                        </p>

                   <!--   <div class="px-2 pt-5 mx-auto">
                                    <div class="g-recaptcha" data-sitekey="<?php //echo Config::GoogleRecaptcha()->google_captcha_key; ?>"
                                        data-callback="enableSubmit" data-theme="dark"></div>
                                </div> -->

                        <p>

                            <button type="submit" class="btn btn-default center-block" name="send">Submit</button>
                    </form>
                    </p>
		            </form>
                </div>
                <!--special end-->
            </div>
            <!--col6 end-->
        </div>
        <br />
        <br />
    </div>
    <!--planbg end-->
    <div class="clearfix"></div>
    <div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3444.856141546528!2d78.05103541455831!3d30.29815678179063!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390929087a1f3545%3A0xd3c934e805d47a14!2sNishima+Complex%2C+Nehru+Colony+Rd%2C+Nehru+Colony%2C+Drone+Puri%2C+Dharampur%2C+Dehradun%2C+Uttarakhand+248001!5e0!3m2!1sen!2sin!4v1549614640067" width="100%" height="390" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!--map end-->
    <div class="clearfix"></div>





    <?php include_once INCLUDE_PATH ."/footer.php"; ?>


    </div><!--containerfluid end-->


    <?php include_once INCLUDE_PATH ."/fscript.php"; ?>
  
  
</body>

</html>
