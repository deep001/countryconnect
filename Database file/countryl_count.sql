-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 21, 2019 at 12:39 AM
-- Server version: 10.3.14-MariaDB-1:10.3.14+maria~bionic
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `countryl_count`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `user_type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `password`, `email_id`, `user_type`) VALUES
(1, 'admin', 'admin', 'admin123', 'Deepaknautiyal52@gmail.com', 'super-admin'),
(13, 'Glenn Corpos', 'Glenntc', 'glennpogi', 'glennvp1@gmail.com', 'super-admin'),
(15, 'Benjie Javier Jr', 'Benjie', 'Benjie@8888', 'benjievp@hotmail.com', 'super-admin'),
(19, 'kayla', 'kayla', 'kaylapretty', 'kayla@parkingbees.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `Airportparking`
--

CREATE TABLE `Airportparking` (
  `airid` int(10) NOT NULL,
  `airparkingname` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `linkname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Airportparking`
--

INSERT INTO `Airportparking` (`airid`, `airparkingname`, `image`, `linkname`) VALUES
(1, 'Alah Airport(AAV)', 'AntipoloCityMontage2012.png', 'Alah Airport'),
(2, 'Awang Airport(CBO)', 'Allan_Jay_Quesada-_DSC_0706_Cebu_Provincial_Capitol_Building,_Cebu_City.JPG', 'Awang Airport'),
(3, 'Bacolod Airport(BCD)', 'Batangasjf9369_14.JPG', 'Bacolod Airport'),
(4, 'baganga Airport(BNQ)', 'cau12.jpg', 'baganga Airport'),
(5, 'Baler Airport(BQA)', 'BacolodCollageByMcLovintosh.jpg', 'Baler Airport'),
(6, 'Basco Airport(BSO)', 'sm.jpg', 'Basco Airport'),
(7, 'Bislig Airport(BPH)', 'index.jpg', 'Bislig Airport'),
(8, 'Busuanga Airport(USU)', 'davao.jpg', 'Busuanga Airport'),
(9, 'Buayan Airport(GES)', 'olongapo.jpg', 'Buayan Airport'),
(10, 'Butuan Airport(BXU)', 'Lapu-lapu City Hotel.jpg', 'Butuan Airport'),
(11, 'Cagayan De Sulu Airport(CDY)', 'SM_Pampanga_By_The_Road.jpg', 'Cagayan De Sulu Airport'),
(12, 'Calbayog Airport(CYP)', 'palawan.jpg', 'Calbayog Airport'),
(13, 'Casiguran Airport(CGG)', 'gensan.jpg', 'Casiguran Airport'),
(14, 'Cauayan Airport(CYZ)', 'iloilo.jpg', 'Cauayan Airport'),
(15, 'Coran Airport(XCN)', 'zamboanga.jpg', 'Coran Airport'),
(16, 'Culion Airport(CUJ)', 'dagupan.jpg', 'Culion Airport'),
(17, 'Cuyo Airport(CYU)', 'tagaytay.jpg', 'Cuyo Airport'),
(18, 'Camarines Norte Airport(DTE)', 'cagayan de oro.jpg', 'Camarines Norte Airport'),
(19, 'Diosdado Macapagal International', 'valenzuela.jpg', 'Diosdado Macapagal International'),
(20, 'Dilasag Airport(DSG)', 'paranaque.jpg', 'Dilasag Airport');

-- --------------------------------------------------------

--
-- Table structure for table `amenities_ico`
--

CREATE TABLE `amenities_ico` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookparking`
--

CREATE TABLE `bookparking` (
  `bookingid` int(10) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `parkingid` int(10) NOT NULL,
  `salerid` int(10) DEFAULT NULL,
  `status` int(255) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookparking`
--

INSERT INTO `bookparking` (`bookingid`, `user_id`, `parkingid`, `salerid`, `status`) VALUES
(1, 70, 11905, 20, 0),
(2, NULL, 11904, NULL, 1),
(3, 87, 11900, 20, 0),
(4, NULL, 148, NULL, 1),
(5, NULL, 156, NULL, 1),
(6, NULL, 153, NULL, 1),
(7, NULL, 159, NULL, 1),
(8, NULL, 152, NULL, 1),
(9, NULL, 157, NULL, 1),
(10, NULL, 150, NULL, 1),
(11, NULL, 144, NULL, 1),
(12, NULL, 142, NULL, 1),
(13, NULL, 151, NULL, 1),
(14, NULL, 149, NULL, 1),
(15, NULL, 145, NULL, 1),
(16, NULL, 155, NULL, 1),
(17, NULL, 147, NULL, 1),
(18, NULL, 154, NULL, 1),
(19, NULL, 146, NULL, 1),
(20, NULL, 143, NULL, 1),
(21, NULL, 11916, NULL, 1),
(22, NULL, 11909, NULL, 1),
(23, NULL, 11891, NULL, 1),
(24, NULL, 11906, NULL, 1),
(25, NULL, 11894, NULL, 1),
(26, NULL, 11897, NULL, 1),
(27, NULL, 11911, NULL, 1),
(28, NULL, 11892, NULL, 1),
(29, NULL, 11913, NULL, 1),
(30, NULL, 11905, NULL, 1),
(31, NULL, 11900, NULL, 1),
(32, NULL, 11907, NULL, 1),
(33, NULL, 11914, NULL, 1),
(34, NULL, 11901, NULL, 1),
(35, NULL, 11903, NULL, 1),
(36, NULL, 11908, NULL, 1),
(37, NULL, 11912, NULL, 1),
(38, NULL, 11893, NULL, 1),
(39, NULL, 11898, NULL, 1),
(40, NULL, 11899, NULL, 1),
(41, NULL, 11890, NULL, 1),
(42, NULL, 11902, NULL, 1),
(43, NULL, 11915, NULL, 1),
(44, NULL, 11895, NULL, 1),
(45, NULL, 11896, NULL, 1),
(46, NULL, 140, NULL, 1),
(47, NULL, 112, NULL, 1),
(48, NULL, 106, NULL, 1),
(49, NULL, 116, NULL, 1),
(50, 87, 124, 24, 0),
(51, NULL, 109, NULL, 1),
(52, NULL, 98, NULL, 1),
(53, NULL, 135, NULL, 1),
(54, NULL, 134, NULL, 1),
(55, NULL, 108, NULL, 1),
(56, NULL, 113, NULL, 1),
(57, NULL, 105, NULL, 1),
(58, NULL, 137, NULL, 1),
(59, NULL, 101, NULL, 1),
(60, NULL, 103, NULL, 1),
(61, NULL, 133, NULL, 1),
(62, NULL, 104, NULL, 1),
(63, NULL, 99, NULL, 1),
(64, NULL, 107, NULL, 1),
(65, NULL, 118, NULL, 1),
(66, NULL, 136, NULL, 1),
(67, NULL, 141, NULL, 1),
(68, NULL, 102, NULL, 1),
(69, NULL, 11931, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(10) NOT NULL,
  `city` varchar(255) NOT NULL,
  `cityimage` varchar(255) NOT NULL,
  `citybannerimage` varchar(255) NOT NULL,
  `city_typ` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `cityimage`, `citybannerimage`, `city_typ`) VALUES
(1, 'Caloocan', 'caloocan.jpg', 'logo_caloocan.png', ''),
(2, 'Las Pinas', 'las pinas.jpg', 'logo_laspinas.jpg', 'F'),
(3, 'Makati', 'makati.jpg', 'logo_makati.jpg', 'S'),
(4, 'Malabon', 'malabon.jpg', 'logo_malabon.jpg', 'F'),
(5, 'Mandaluyong', 'mandaluyong.jpg', 'logo_mandalayong.png', 'S'),
(6, 'Manila', 'manila.jpg', 'logo_Manila.png', 'S'),
(7, 'Marikina', 'marikina.jpg', 'logo_marikina.png', 'F'),
(8, 'Muntinlupa', 'muntinlupa.jpg', 'logo_muntinlupa.jpg', 'F'),
(9, 'Navotas', 'navotas.jpg', 'logo_navotas.jpg', 'F'),
(10, 'Paranaque', 'paranaque.jpg', 'logo_paranaque.png', 'F'),
(11, 'Pasay', 'pasay.jpg', 'logo_pasay.jpg', 'S'),
(12, 'Pasig', 'pasig.jpg', 'logo_pasig.jpg', 'S'),
(13, 'Quezon', 'Quezon.jpg', 'logo_QC.png', 'S'),
(14, 'Taguig', 'taguig.jpg', 'logo_taguig.jpg', 'S'),
(15, 'Valenzuela', 'valenzuela.jpg', 'logo_valenzuela.png', 'F'),
(20, 'Metro Davao', 'davao.jpg', 'logo_Davao.png', 'S'),
(19, 'Metro Cebu', 'cebu.jpg', 'logo_cebu.png', 'S'),
(17, 'Baguio', 'baguio.jpg', 'logo_baguio.png', ''),
(21, 'Cagayan de Oro', 'cagayan de oro.jpg', 'logo_cdo.jpg', 'F'),
(22, 'Tagaytay', 'tagaytay.jpg', 'logo_tagaytay.jpg', 'F'),
(23, 'Dagupan', 'dagupan.jpg', 'logo_dagupan.jpg', 'F'),
(24, 'Zamboanga', 'zamboanga.jpg', 'logo_zambo.png', 'F'),
(25, 'Iloilo', 'iloilo.jpg', 'logo_iloilo.jpg', 'F'),
(26, 'General Santos', 'gensan.jpg', 'logo_gensan.jpg', 'F'),
(27, 'Puerto Princesa', 'palawan.jpg', 'logo_puerto_princesa.jpg', 'F'),
(33, 'Laoag', 'index.jpg', 'laoag banenr.jpg', 'F'),
(35, 'Bacolod', 'BacolodCollageByMcLovintosh.jpg', 'Ph_seal_negros_occidental_bacolodv2.jpg', 'F'),
(34, 'Naga', 'sm.jpg', 'Naga_Camarines_Sur.png', 'F'),
(32, 'Davao', 'davao.jpg', 'Davao_City_Ph_official_seal.png', 'F'),
(28, 'Clark', 'SM_Pampanga_By_The_Road.jpg', 'Ph_seal_angeles,_pampanga.png', 'F'),
(30, 'Lapu-lapu', 'Lapu-lapu City Hotel.jpg', 'Lapu-Lapu_Cebu.png', 'F'),
(31, 'Olongapo', 'olongapo.jpg', 'seal_olongapo.png', 'F'),
(36, 'Cauayan', 'cau12.jpg', 'Official_Seal_of_the_City_of_Cauayan.jpg', 'F'),
(37, 'San Juan', 'Batangasjf9369_14.JPG', 'San_Juan_Batangas.png', 'F'),
(38, 'Cebu', 'Allan_Jay_Quesada-_DSC_0706_Cebu_Provincial_Capitol_Building,_Cebu_City.JPG', 'Ph_seal_cebu.png', 'F'),
(39, 'Antipolo', 'AntipoloCityMontage2012.png', 'Antipolo-logo-copy.jpg', 'F'),
(42, 'Eastwood ', 'Eastwood City.jpg', 'eastwood.jpg', 'S'),
(43, 'Trece Martires', '1024px-TreceMartiresjf0078_10.JPG', 'Ph_seal_cavite_trece_martires_city.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_dp`
--

CREATE TABLE `client_dp` (
  `cli_ed_id` int(255) NOT NULL,
  `cli_ed_pri` int(255) NOT NULL,
  `clien_dp` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_google_account`
--

CREATE TABLE `client_google_account` (
  `id` int(255) NOT NULL,
  `oauth_provider` varchar(255) NOT NULL,
  `oauth_uid` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_google_account`
--

INSERT INTO `client_google_account` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture`, `link`, `created`, `modified`) VALUES
(2, 'google', '107036251170544710185', 'Benigno', 'Javier Jr.', 'benjievp@gmail.com', '', 'en', 'https://lh6.googleusercontent.com/-Fil6q9A9MjI/AAAAAAAAAAI/AAAAAAAABTA/z9Hz9evvW54/photo.jpg', 'https://plus.google.com/107036251170544710185', '2017-01-14 07:17:48', '2017-04-01 12:45:03'),
(6, 'google', '116417372747870082214', 'Divesh', 'Kumar', 'divesh20july@gmail.com', '', 'en', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 'https://plus.google.com/116417372747870082214', '2017-01-17 13:12:54', '2017-01-17 13:13:09'),
(7, 'google', '105364262336252593731', 'Benjie', 'Javier', 'virtualtok@gmail.com', '', 'en', 'https://lh3.googleusercontent.com/-1YUz5vgkq0Q/AAAAAAAAAAI/AAAAAAAAAA0/KayHQ_uRn2Q/photo.jpg', 'https://plus.google.com/105364262336252593731', '2017-02-10 10:04:22', '2017-02-10 10:31:14'),
(8, 'google', '112835472904323079678', 'Hitesh', 'Rawat', 'hitesh@rankmantra.com', '', 'en-GB', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', '2017-02-18 12:14:25', '2017-02-18 12:31:27'),
(9, 'google', '113421516150825986153', 'Kayla', 'Villaroman', 'kaylamvillaroman@gmail.com', '', 'en', 'https://lh6.googleusercontent.com/-FW0CL1D2Okk/AAAAAAAAAAI/AAAAAAAAAGU/3rX6Cd0Wu3Q/photo.jpg', 'https://plus.google.com/113421516150825986153', '2017-03-28 10:32:08', '2017-06-16 04:06:58'),
(10, 'google', '116153891428073598106', 'nelg', 'ria', 'nelgria@gmail.com', '', 'en', 'https://lh6.googleusercontent.com/-uvNOrx6KYZw/AAAAAAAAAAI/AAAAAAAABFA/sHZaOYOmnho/photo.jpg', 'https://plus.google.com/116153891428073598106', '2017-03-28 10:51:10', '2017-03-28 10:52:24'),
(11, 'google', '101490169013824167065', 'DIVESH', 'KUMAR', 'diveshk442@gmail.com', '', 'en', 'https://lh3.googleusercontent.com/-rXN6RKIWtuU/AAAAAAAAAAI/AAAAAAAAAtc/16cGt1tra24/photo.jpg', 'https://plus.google.com/101490169013824167065', '2017-04-13 10:19:17', '2017-06-07 10:03:46'),
(12, 'google', '109059190438472574308', 'Emil', 'Arcilla', 'emilarcilla17@gmail.com', '', 'en', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', '', '2017-12-08 03:49:42', '2017-12-08 03:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `clie_google_acc`
--

CREATE TABLE `clie_google_acc` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clie_google_acc`
--

INSERT INTO `clie_google_acc` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture`, `link`, `created`, `modified`) VALUES
(2, 'google', '117098389172462157964', 'Deepak', 'Nautiyal', 'deepaknautiyal52@gmail.com', '', 'en', 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 'https://plus.google.com/117098389172462157964', '2017-01-04 12:58:51', '2017-01-04 12:58:51'),
(4, 'google', '107036251170544710185', 'Benigno', 'Javier Jr.', 'benjievp@gmail.com', '', 'en', 'https://lh6.googleusercontent.com/-Fil6q9A9MjI/AAAAAAAAAAI/AAAAAAAABTA/z9Hz9evvW54/photo.jpg', 'https://plus.google.com/107036251170544710185', '2017-01-08 08:06:34', '2017-07-26 12:07:22'),
(7, 'google', '113421516150825986153', 'Kayla', 'Villaroman', 'kaylamvillaroman@gmail.com', '', 'en', 'https://lh6.googleusercontent.com/-FW0CL1D2Okk/AAAAAAAAAAI/AAAAAAAAAGU/3rX6Cd0Wu3Q/photo.jpg', 'https://plus.google.com/113421516150825986153', '2017-03-28 10:34:29', '2017-06-16 04:14:16'),
(8, 'google', '105364262336252593731', 'Benjie', 'Javier', 'virtualtok@gmail.com', '', 'en', 'https://lh3.googleusercontent.com/-1YUz5vgkq0Q/AAAAAAAAAAI/AAAAAAAAAA0/KayHQ_uRn2Q/photo.jpg', 'https://plus.google.com/105364262336252593731', '2017-04-04 14:41:09', '2017-04-04 14:41:24'),
(9, 'google', '101490169013824167065', 'DIVESH', 'KUMAR', 'diveshk442@gmail.com', '', 'en', 'https://lh3.googleusercontent.com/-rXN6RKIWtuU/AAAAAAAAAAI/AAAAAAAAAtc/16cGt1tra24/photo.jpg', 'https://plus.google.com/101490169013824167065', '2017-06-05 07:32:52', '2017-06-05 07:55:21');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `iso_code_2` varchar(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `iso_code_3` varchar(3) COLLATE utf8_bin NOT NULL DEFAULT '',
  `address_format` text COLLATE utf8_bin NOT NULL,
  `postcode_required` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegowina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TP', 'TMP', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(73, 'France', 'FR', 'FRA', '', 0, 1),
(74, 'France, Metropolitan', 'FX', 'FXX', '', 0, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'Macedonia', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '', 0, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(236, 'Yugoslavia', 'YU', 'YUG', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `edit_img_partner`
--

CREATE TABLE `edit_img_partner` (
  `edit_id` int(255) NOT NULL,
  `du_id` int(255) NOT NULL,
  `edi_img` varchar(255) NOT NULL,
  `cit_img` varchar(255) NOT NULL,
  `status1` int(255) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fileimages`
--

CREATE TABLE `fileimages` (
  `imageid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `fileimages` varchar(150) NOT NULL,
  `DateofSubmission` varchar(50) NOT NULL,
  `TimeoSubmission` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fileimages`
--

INSERT INTO `fileimages` (`imageid`, `userid`, `fileimages`, `DateofSubmission`, `TimeoSubmission`) VALUES
(1, 53, 'bnr1316024256Desert.jpg', '', ''),
(4, 53, 'bnr46937358Koala.jpg', '', ''),
(5, 0, 'bnr2140711626Kennedy.jpg', '', ''),
(6, 53, 'bnr15814290141.jpg', '', ''),
(8, 53, 'bnr204662381Resume_Deepak (1).doc', '', ''),
(9, 53, 'bnr377392159Team Lead-JD.pdf', '', ''),
(10, 53, 'bnr44110309533.jpg', '2016-06-09', '09:48:11'),
(11, 53, 'bnr183023836Penguins.jpg', '2016-06-09', '12:15:59'),
(12, 0, 'bnr2096223912Beach yoga 2.jpg', '2016-07-08', '06:16:06'),
(13, 55, 'bnr602008608Koala.jpg', '2016-07-28', '08:04:00'),
(14, 55, 'bnr763969228Hydrangeas.jpg', '2016-07-28', '08:06:22'),
(17, 55, 'xx.mp4', '2016-07-28', '11:57:03'),
(20, 55, '01 Daddy Mummy (Bhaag Johnny) 190Kbps.mp3', '2016-08-04', '11:52:10'),
(19, 55, 'oo.mp4', '2016-07-28', '12:27:40'),
(21, 55, '01 Gulaabo (Shaandaar) -190Kbps.mp3', '2016-08-05', '11:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `fileonlyimages`
--

CREATE TABLE `fileonlyimages` (
  `imageid` int(10) NOT NULL,
  `userid` int(10) NOT NULL,
  `fileimages` varchar(50) NOT NULL,
  `DateofSubmission` varchar(50) NOT NULL,
  `TimeoSubmission` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fileonlyimages`
--

INSERT INTO `fileonlyimages` (`imageid`, `userid`, `fileimages`, `DateofSubmission`, `TimeoSubmission`) VALUES
(1, 53, 'bnr129355701722.jpg', '', ''),
(2, 53, 'bnr124144038633.jpg', '2016-06-09', '07:33:55'),
(3, 53, 'bnr143091564222.jpg', '2016-06-09', '08:17:00'),
(4, 53, 'bnr539874658Beach yoga 2.jpg', '2016-07-19', '11:44:19'),
(5, 55, 'bnr117501137Penguins.jpg', '2016-07-28', '08:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `fillevent`
--

CREATE TABLE `fillevent` (
  `eventid` int(20) NOT NULL,
  `Date` varchar(50) NOT NULL,
  `Eventdetail` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fillevent`
--

INSERT INTO `fillevent` (`eventid`, `Date`, `Eventdetail`) VALUES
(1, '2016-06-15', 'world heath day'),
(2, '2016-06-16', 'sasss'),
(3, '2016-06-15', 'fv'),
(4, '2016-06-15', 'sadsa'),
(5, '2016-06-15', 'gfdg'),
(6, '2016-06-15', 'World health day');

-- --------------------------------------------------------

--
-- Table structure for table `newmember`
--

CREATE TABLE `newmember` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `bio` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `new_park`
--

CREATE TABLE `new_park` (
  `saerid` int(255) DEFAULT NULL,
  `traction_id` varchar(255) DEFAULT NULL,
  `uni_parking_code` varchar(255) DEFAULT NULL,
  `Parkots` varchar(10) DEFAULT NULL,
  `UpldImage` varchar(100) NOT NULL,
  `parngname` varchar(500) NOT NULL,
  `parkgcategory` varchar(255) NOT NULL,
  `parkgdescription` varchar(50) NOT NULL,
  `howtetthere` text NOT NULL,
  `InialRate` varchar(20) NOT NULL,
  `Raterhour` varchar(20) NOT NULL,
  `parng_rate_details` varchar(255) NOT NULL,
  `Amenties` varchar(100) NOT NULL,
  `stats` int(11) NOT NULL DEFAULT 0,
  `parngstatus` varchar(30) NOT NULL,
  `actation` varchar(255) DEFAULT NULL,
  `ciy` varchar(50) NOT NULL,
  `ciimage` varchar(50) DEFAULT NULL,
  `locion` varchar(500) NOT NULL,
  `lgsa` varchar(255) NOT NULL,
  `lt` varchar(255) NOT NULL,
  `pingdatefrom` varchar(50) NOT NULL,
  `paingdateto` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Parkingbysale`
--

CREATE TABLE `Parkingbysale` (
  `parkingid` int(10) NOT NULL,
  `salerid` int(10) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `unique_parking_code` varchar(255) NOT NULL,
  `ParkingSlots` varchar(10) NOT NULL,
  `UploadImage` varchar(100) NOT NULL,
  `parkingname` varchar(500) NOT NULL,
  `parkingcategory` varchar(255) NOT NULL,
  `parkingsubcategory` text NOT NULL,
  `parkingdescription` varchar(50) NOT NULL,
  `howtogetthere` text NOT NULL,
  `InitialRate` varchar(20) NOT NULL,
  `Rateperhour` varchar(20) DEFAULT NULL,
  `parking_rate_details` varchar(255) DEFAULT NULL,
  `Amenities` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `parkingstatus` varchar(30) NOT NULL,
  `activation` varchar(255) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `cityimage` varchar(50) DEFAULT NULL,
  `location` varchar(500) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `parkingdatefrom` varchar(50) DEFAULT NULL,
  `parkingdateto` varchar(50) DEFAULT NULL,
  `parkingtimefrom` varchar(255) DEFAULT NULL,
  `parkingtimeto` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Parkingbysale`
--

INSERT INTO `Parkingbysale` (`parkingid`, `salerid`, `transaction_id`, `unique_parking_code`, `ParkingSlots`, `UploadImage`, `parkingname`, `parkingcategory`, `parkingsubcategory`, `parkingdescription`, `howtogetthere`, `InitialRate`, `Rateperhour`, `parking_rate_details`, `Amenities`, `status`, `parkingstatus`, `activation`, `city`, `cityimage`, `location`, `lng`, `lat`, `parkingdatefrom`, `parkingdateto`, `parkingtimefrom`, `parkingtimeto`) VALUES
(97, 10, 'PARKING1709880640', '58180eacbb04c', '10', 'Parking325436446park1.jpg', 'Chowking Ortigas Parking', 'Restaurants Parking', '', 'Parking Slot 7', 'park beside Mcdonalds Shell', '30', '', 'First 2 hours', 'Valet,SelfPark,CoveredParking,Carwash', 1, 'Activated', NULL, 'Pasig', NULL, 'Ortigas Center, Pasig, NCR, Philippines', '121.059675', '14.583771', '01-01-1970', '01-01-1970', '1484330400', '1484334000'),
(93, 10, 'PARKING23233636', '58141e0255c10', '2', 'Parking1140914060park4.jpg', 'Corinthian Executive Parking', 'Office Building Parking', '', 'Slot 51 & 56', 'Entrance of parking is along Sapphire road, at the back of Robinsons Galleria', '50', '', '1', 'SelfPark,CoveredParking,On-Site Staff,>In and Out Privileges,Elevator Access', 1, 'Activated', NULL, 'Pasig', NULL, 'Robinsons Galleria, Ortigas Avenue, Quezon City, NCR, Philippines', '121.058949', '14.591892', '29-10-2016', '05-11-2016', '1484254800', '1484258400'),
(96, 20, 'PARKING1381962524', '58157b8a9163b', '2', 'Parking729689872P_20161022_161352_HDR.jpg', 'NFINITE IT PARKING', 'Office Building Parking', '', 'Twin Parking - Parking 51 and 52', 'By Car. Drive till you reach Robinsons Galeria, once in, look for Corinthian Executive Regency Condo and Park.', '45', '10', 'First 3 Hours', 'CoveredParking,Elevator Access', 1, 'Activated', NULL, 'Pasig', NULL, 'Corinthian Executive Regency, Ortigas Avenue, Pasig, NCR, Philippines', '121.06168780000007', '14.589927', '01-11-2016', '01-11-2016', '1484229600', '1484233200'),
(103, 20, 'PARKING1018237931', '58182de095e20', '5', 'Parking17725318022016-06-03 12.17.11.jpg', 'Holy Child College Parking', 'Universities AND School Parking', '', 'Open parking good for 5 spot', 'Drive to Holy Child College, Parkingbees parking is in front of Schools Main Building fronting BDO Bank.', 'FREE PARKING', '', '', 'SelfPark,CoveredParking', 1, 'Activated', NULL, 'Metro Davao', NULL, 'Holy Child College, Jacinto Street, Davao City, Davao Region, Philippines', '125.59928660000003', '7.0587198', '', '', '', ''),
(110, 20, 'PARKING1863693379', '581861f407fb7', '10', 'Parking1541037291A PLACE parking.JPG', 'A PLACE Parking', 'Office Building Parking', '', 'Premium Parking good for 10 slots', 'Drive to SM MOA Arena thru Coral Way, Pasay. ', '40', '20', 'First 3 Hours', 'SelfPark,CoveredParking,Overnight Parking,>In and Out Privileges,Elevator Access', 1, 'Activated', NULL, 'Pasay', NULL, 'A Place, Coral Way, Pasay, NCR, Philippines', '120.98724019999997', '14.5312175', '01-11-2016', '30-11-2016', '1484254800', '1484258400'),
(123, 24, 'PARKING1337784274', '582296a61a0d4', '112', 'Parking1780370161Marco Polo_NO.jpg', 'PARKWISE Marco Polo Basement', 'Hotel Parking', '', 'Parking minimum of 112 slots', 'Drive to Ortigas Center, Parkwise parking is located in Marco Polo Hotel basement', '60', '20', 'First 2 hours', 'SelfPark,CoveredParking,Open 24/7', 1, 'Activated', NULL, 'Pasig', NULL, 'Marco Polo Ortigas Manila, Pasig, NCR, Philippines', '121.06371220000005', '14.5874402', '01-11-2016', '30-11-2016', '1484240400', '1484244000'),
(142, 20, 'PARKING30246901', '583fbaa99ccfe', '25', 'Parking514411358mactan-cebu.jpg', 'Mactan-Cebu International Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to Mactan Cebu International Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'Lapu-lapu', NULL, 'Mactan-Cebu International Airport, Lapu-Lapu City, Philippines', '123.9802214', '10.3106556', '', '', '', ''),
(143, 20, 'PARKING806439983', '583fbd65c15b7', '25', 'Parking1897477139Nice airports in the Philippines (4).jpg', 'Ninoy Aquino International Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to Ninoy Aquino International Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'Pasay', NULL, 'Ninoy Aquino International Airport, Andrews Avenue, Pasay, NCR, Philippines', '121.01650799999993', '14.5122739', '', '', '', ''),
(140, 20, 'PARKING881590115', '5833e35807ca7', '25', 'Parking805076256IMG_20130222_162919.jpg', ' Diosdado Macapagal International Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to  Diosdado Macapagal International Airport (Clark International Airport) and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Overnight Parking,Open 24/7', 1, 'Activated', NULL, 'Clark City', NULL, '2010 Mabalacat Road, Clark Freeport, Mabalacat, Pampanga, Central Luzon, Philippines', '120.50756449999994', '15.1903943', '', '', '', ''),
(141, 20, 'PARKING1189135323', '583d18e46daa6', '25', 'Parking2061426767viber image.jpg', 'Iloilo International Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to Iloilo International Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Overnight Parking,Open 24/7', 1, 'Activated', NULL, 'Iloilo', NULL, 'Iloilo International Airport, Cabatuan, Western Visayas, Philippines', '122.49620729999992', '10.8315005', '', '', '', ''),
(144, 20, 'PARKING113090519', '583fc666e47c8', '25', 'Parking86348550SUBIC.jpg', 'Subic Bay International Airport Parking', 'Airport Parking', 'Awang Airport', 'Airport Parking', 'Drive to Subic Bay International Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'Olongapo', NULL, 'Subic Bay International Airport, Subic Bay Freeport Zone, Central Luzon, Philippines', '120.28286270000001', '14.795414', '', '', '', ''),
(145, 20, 'PARKING2048194396', '583fcce14c073', '25', 'Parking1312577310davao-airport-960x720.jpg', 'Francisco Bangoy International Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to Davao International Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'Davao', NULL, 'Davao International Airport, Davao City, Philippines', '125.64558369999997', '7.1296803', '', '', '', ''),
(146, 20, 'PARKING1273954922', '583fd1bc08f0c', '25', 'Parking913337373GENERAL SANTOS.jpg', 'General Santos International Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to General Santos International Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,BeeScan,Overnight Parking', 1, 'Activated', NULL, 'General Santos', NULL, 'General Santos International Airport Terminal, General Santos City, SOCCSKSARGEN, Philippines', '125.10014260000003', '6.0576737', '', '', '', ''),
(147, 20, 'PARKING1616251826', '583fd55e1b8eb', '25', 'Parking801483270laoag.jpg', 'Laoag International Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to Laoag International Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'Laoag', NULL, 'Laoag International Airport, Laoag City, Ilocos Region, Philippines', '120.53357789999995', '18.1820211', '', '', '', ''),
(148, 20, 'PARKING1567161671', '583fd86a951e0', '25', 'Parking1200486987International_Airport_of_Zamboanga_City,_Sta._Maria,.jpg', 'Zamboanga International Airport Parking', 'Airport Parking', 'Alah Airport', 'Airport Parking', 'Drive to Zamboanga International Airport and parkingbees is located beside the airport.	', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'Zamboanga', NULL, 'Zamboanga International Airport, Zamboanga, Philippines', '122.061647', '6.919884', '', '', '', ''),
(150, 20, 'PARKING1755493676', '584026869cf07', '25', 'Parking1424584583bacolod.jpg', 'Bacolod City Domestic Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to Bacolod City Domestic Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Overnight Parking,Open 24/7', 1, 'Activated', NULL, 'Bacolod', NULL, 'Philippine Airlines - Bacolod Airport Ticket Office, Silay City, Western Visayas, Philippines', '123.01501800000005', '10.778497', '', '', '', ''),
(151, 20, 'PARKING920368833', '584175c9c4c2e', '25', 'Parking1490646612PuertoPrincesa_Airport.JPG', 'Puerto Princesa Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to Puerto Princesa Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Overnight Parking,Open 24/7', 1, 'Activated', NULL, 'Puerto Princesa', NULL, 'Puerto Princesa International Airport, Rizal Avenue, Puerto Princesa, MIMAROPA, Philippines', '118.75693279999996', '9.740318', '', '', '', ''),
(152, 20, 'PARKING1409710796', '584177cb500c0', '25', 'Parking1508195525Cauayan-Airport-1.jpg', 'Cauayan Airport Parking', 'Airport Parking', '', 'Airport Parking', 'Drive to Cauayan Airport and parkingbees is located beside the airport.', 'FREE PARKING', '', '', 'Restrooms Available,Overnight Parking,Open 24/7', 1, 'Activated', NULL, 'Cauayan', NULL, 'Cauayan Airport, Airport Road, Cauayan City, Philippines', '121.75694299999998', '16.927778', '', '', '', ''),
(159, 20, 'PARKING812517886', '5848bd124ef06', '25', 'Parking869597725s_01-1024x419.jpg', 'Boso-Boso Highlands Resort And Hotel Parking', 'Hotel Parking', '', 'Hotel Parking', 'Drive to Boso-Boso Highlands Resort and Hotel, parkingbees is inside the resort.', 'FREE PARKING', '', '', 'Restrooms Available,Overnight Parking,Open 24/7', 1, 'Activated', NULL, 'Antipolo', NULL, 'Boso Boso Resort, Antipolo, Calabarzon, Philippines', '121.19466069999999', '14.6212414', '', '', '', ''),
(11890, 20, 'PARKING1961456836', '584e3853a97fa', '25', 'Parking1858901841picc.jpg', 'Philippine International Convention Center Parking', 'Theaters AND Venues Parking', '', 'Street Parking', 'Drive to Philippine International Convention Center and street parking is located outside the building.', 'FREE PARKING', '', '', 'Open 24/7', 1, 'Activated', NULL, 'Pasay', NULL, 'PICC, Vicente Sotto Street, Pasay, NCR, Philippines', '120.98260000000005', '14.554972', '', '', '', ''),
(11891, 20, 'PARKING303496032', '584e3d2764f3f', '25', 'Parking1266409947blue bay walk.jpg', 'Blue Bay Walk Parking', 'Restaurants Parking', '', 'Restaurant Parking', 'Drive to Blue Bay Walk and parking is located outside the building.', 'FREE PARKING', '', '', 'Open 24/7', 1, 'Activated', NULL, 'Pasay', NULL, 'Blue Bay Walk, Diosdado Macapagal Boulevard, Pasay, NCR, Philippines', '120.98969239999997', '14.5387376', '', '', '', ''),
(11904, 20, 'PARKING648811148', '5852110b726e6', '25', 'Parking1649686451steves.jpg', 'Steve\'s Barbecue Parking', 'Restaurants Parking', '', 'Open Parking', 'Drive to Steve\'s Barbecue and parkingbees is located outside the restaurant.', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'Cagayan de Oro', NULL, 'J.R. Borja Street, Cagayan de Oro, Misamis Oriental, Northern Mindanao, Philippines', '124.64538170000003', '8.4788079', '', '', '', ''),
(11905, 20, 'PARKING1482652644', '58535470c9d85', '25', 'Parking1087546372razons.jpg', 'Razon\'s Halo-Halo San Fernando Pampanga Parking', 'Restaurants Parking', '', 'Restaurant Parking', 'Drive to Razon\'s Halo-Halo San Fernando Pampanga and parkingbees is located outside the restaurant. (edit city)', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'San Juan', NULL, ' B. Mendoza, Sto. Rosaria, San Fernando, 2000 Pampanga ', '120.69835420000004', '15.0500768', '', '', '', ''),
(11906, 20, 'PARKING959159677', '58576c5a487d3', '25', 'Parking1897214695bmw.jpg', 'BMW San Fernando Pampanga Parking', 'Office Building Parking', '', 'Open Parking', 'Drive to BMW San Fernando Pampanga and parkingbees is located in front of the building. (edit city)', 'FREE PARKING', '', '', 'Restrooms Available', 1, 'Activated', NULL, 'San Juan', NULL, 'Premier Cars BMW Pampanga, Jose Abad Santos Avenue, San Fernando, Central Luzon, Philippines', '120.68977380000001', '15.0457822', '', '', '', ''),
(11908, 20, 'PARKING146248229', '58577578d273d', '25', 'Parking275862619the coffeebean.jpg', 'The Coffee Bean & Tea Leaf - Tomas Morato Parking', 'Restaurants Parking', '', 'Restaurant Parking', 'Drive to The Coffee Bean & Tea Leaf - Tomas Morato and parkingbees is located in front of the building. \r\n', 'FREE PARKING', '', '', 'Restrooms Available', 1, 'Activated', NULL, 'Quezon', NULL, 'The Coffee Bean & Tea Leaf - Tomas Morato, Tomas Morato Avenue, Quezon City, NCR, Philippines', '121.0346604', '14.6321609', '', '', '', ''),
(11909, 20, 'PARKING1049636554', '585776f742ae7', '25', 'Parking1820911512congo grill.jpg', 'Congo Grille Parking', 'Restaurants Parking', '', 'Restaurant Parking', 'Drive to Congo Grille and parkingbees is located outside the building.\r\n', 'FREE PARKING', '', '', 'Restrooms Available', 1, 'Activated', NULL, 'Quezon', NULL, 'Congo Grille - Tomas Morato, Quezon City, NCR, Philippines', '121.03486120000002', '14.633454', '', '', '', ''),
(11910, 20, 'PARKING1718455647', '58577822a3a15', '25', 'Parking1876273267alfredos.jpg', 'Alfredo\'s Steak House Parking', 'Restaurants Parking', '', 'Restaurant Parking', 'Drive to Alfredo\'s Steak House and parkingbees is located outside the building.', '50', '', 'First 3 hours', 'Restrooms Available', 1, 'Activated', NULL, 'Quezon', NULL, 'Alfredo\'s Steak House, Tomas Morato Avenue, Quezon City, NCR, Philippines', '121.03432880000003', '14.6307829', '01-12-2017', '12-01-2017', '1484247600', '1484251200'),
(11911, 20, 'PARKING1458426967', '585781419cdc3', '25', 'Parking55258408rcbc.jpg', 'RCBC Savings Bank - Tomas Morato Parking', 'Office Building Parking', '', 'Bank Parking', 'Drive to RCBC Savings Bank - Tomas Morato and parkingbees is located outside the building.\r\n', 'FREE PARKING', '', '', 'Restrooms Available', 1, 'Activated', NULL, 'Quezon', NULL, 'RCBC Savings Bank - Tomas Morato, Quezon City, NCR, Philippines', '121.03387700000007', '14.629096', '', '', '', ''),
(11912, 20, 'PARKING1270691390', '5857839d95588', '25', 'Parking683465274zirkoh2.jpg', 'Zirkoh Bar - Tomas Morato Parking', 'Restaurants Parking', '', 'Restaurant Parking', 'Drive to Zirkoh Bar - Tomas Morato and parkingbees is located outside the building.\r\n', 'FREE PARKING', '', '', 'Restrooms Available', 1, 'Activated', NULL, 'Quezon', NULL, 'Zirkoh Bar - Tomas Morato, Quezon City, NCR, Philippines', '121.03401299999996', '14.629771', '', '', '', ''),
(11913, 20, 'PARKING1579039039', '585785cec1403', '25', 'Parking748626601jcoh.jpg', 'J.CO Donuts, Tomas Morato Avenue Parking', 'Restaurants Parking', '', 'Restaurant Parking', 'Drive to J.CO Donuts, Tomas Morato Avenue and parkingbees is located outside the building. ', 'FREE PARKING', '', '', 'Restrooms Available', 1, 'Activated', NULL, 'Quezon', NULL, 'J.CO Donuts, Tomas Morato Avenue, Quezon City, NCR, Philippines', '121.03493930000002', '14.6332016', '', '', '', ''),
(11915, 20, 'PARKING1272597260', '58578e54dc2d4', '25', 'Parking132522460jolibbee.jpg', 'Jollibee, Tomas Morato Avenue Parking', 'Restaurants Parking', '', 'Restaurant Parking', 'Drive to Jollibee, Tomas Morato Avenue and parkingbees is located outside the building.', 'FREE PARKING', '', '', 'Restrooms Available,Open 24/7', 1, 'Activated', NULL, 'Quezon', NULL, 'Jollibee, Tomas Morato Avenue, Quezon City, NCR, Philippines', '121.03448400000002', '14.6313061', '', '', '', ''),
(11916, 20, 'PARKING1572144618', '5857909f326f2', '25', 'Parking2110440825Quezon City Sports Club.jpg', 'Quezon City Sports Club Parking', 'Stadiums AND Sports Parking', '', 'Sports Parking', 'Drive to Quezon City Sports Club and parkingbees is located outside the building.', 'NON BOOKABLE', '', '', 'SelfPark,Restrooms Available,Overnight Parking,Open 24/7,On-Site Staff,>In and Out Privileges', 1, 'Activated', NULL, 'Quezon', NULL, 'Quezon City Sports Club, East Rodriguez Sr. Avenue, Quezon City, NCR, Philippines', '121.02670109999997', '14.6230536', '', '', '', ''),
(11931, 33, 'PARKING606193914', '587f1cce4121b', '25', 'Parking1857537643world vision.jpg', ' World Vision Philippines Parking', 'Office Building Parking', '', 'Office Parking', 'Drive to World Vision Philippines and parkingbees is located outside the building.', 'FREE PARKING', NULL, NULL, 'Carwash,SelfPark', 1, 'Activated', NULL, 'Quezon', NULL, ' 389 Quezon Ave, Corner West 6th Street, Diliman, Quezon City, 1104 Metro Manila ', '121.027804', '14.638312', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `partn_dp`
--

CREATE TABLE `partn_dp` (
  `par_ed_id` int(255) NOT NULL,
  `par_ed_pri` int(255) NOT NULL,
  `par_dp` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partn_dp`
--

INSERT INTO `partn_dp` (`par_ed_id`, `par_ed_pri`, `par_dp`) VALUES
(21, 33, 'Parking_dp39381837911200628_751779691608561_3803910113212891535_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sellparkinguser`
--

CREATE TABLE `sellparkinguser` (
  `id` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `Lastname` varchar(50) NOT NULL,
  `Firstname` varchar(50) NOT NULL,
  `Mobilenumber` varchar(100) NOT NULL,
  `dp` varchar(255) DEFAULT NULL,
  `status` int(255) NOT NULL DEFAULT 0,
  `cpnumber` varchar(255) NOT NULL,
  `about_part` text NOT NULL,
  `address` text NOT NULL,
  `company` text NOT NULL,
  `activation` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellparkinguser`
--

INSERT INTO `sellparkinguser` (`id`, `email`, `password`, `Lastname`, `Firstname`, `Mobilenumber`, `dp`, `status`, `cpnumber`, `about_part`, `address`, `company`, `activation`) VALUES
(2, 'ndeepak48@gmail.com', 'c2ee1bfdff990ecec62ccd4678747988', 'Nautiyal', 'Deepak', '8979528858', 'dp1498392764avatar-2.jpg', 1, '56773324', 'Individual Seller', 'Clock Tower', '', 'PARTNERS50878269'),
(20, 'bumblebee@parkingbees.com', 'ea33e0ecde28b02b1168378b1f78333c', 'Bumblebee', 'Bees', '8979528858', 'Parking_dp1599174300bumblebee.jpg', 1, '09356773324', 'Individual Seller', 'NF Parking', '', 'PARTNERS5343072'),
(10, 'benjie@ncomputingph.com', 'ea33e0ecde28b02b1168378b1f78333c', 'Javier', 'Benjie', '6406804', 'dp1775005261parkingBee-logo-index.png', 1, '9081250030', 'Individual Seller', 'Corinthian Executive Regency Parking', '', 'PARTNERS15164272'),
(24, 'parkwise2016@gmail.com', '859326e5edf2780592287d6d217a07e8', 'Phils', 'Parkwise', '8979528858', 'Parking_dp524577137P_20161017_171457_1_1_1.jpg', 1, '09356773324', 'Parking Operator or Company', '', 'ParkingBuzz', 'PARTNERS146537013'),
(25, 'benogs@yahoo.com', '61cc0e405f4b518d264c089ac8b642ef', 'Javiers', 'Benjies', '8979528858', NULL, 0, '9278893576', 'Parking Operator or Company', '', 'ParkingBuzz', 'PARTNERS12159597'),
(26, 'nelgria@hotmail.com', 'ea33e0ecde28b02b1168378b1f78333c', 'Corpos', 'Glenn', '6406804', 'dp1639894347', 1, '9356773324', 'Individual Seller', 'NF Parking', '', 'PARTNERS129231727'),
(32, 'ideavirusph@gmail.com', '9f3ffd37c12f658af6cde157f9fcb62f', 'javier', 'Benjie', '6406808', NULL, 1, '09081250030', 'Parking Operator or Company', '', 'ParkingB', 'PARTNERS43059764'),
(33, 'linaflorcalarde@gmail.com', '44acb1fe246bcca71e9eff7f2ca6b0b0', 'calarde', 'lina flor', '', NULL, 1, '+639263448005', 'Individual Seller', 'san jose city', '', 'PARTNERS135467442'),
(37, 'ea_asya@asyadesign.com.ph', '4e740e8be7f39b115fb85b9f3b6a12bc', 'ASYA', 'EA', '808-5888', NULL, 0, '09178090995', 'Individual Seller', 'APLACE CORALWAY', '', 'PARTNERS104603422'),
(36, 'kayla_villaroman@asyadesign.com.ph', 'ef7f876d6d0a7de2e8283add0d75b5f7', 'DESIGN', 'ASYA', '8085888', NULL, 0, '09178090995', 'Individual Seller', 'A PLACE BLDG.', '', ''),
(38, 'executiveassistantasya@gmail.com', '24477cccc35686f79f01e1eab8fdca47', 'Asya', 'Executive Assistant', '8085888', NULL, 0, '09178090995', 'Individual Seller', 'MOA, Manila', '', 'PARTNERS69607501'),
(39, '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', NULL, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `Share`
--

CREATE TABLE `Share` (
  `id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  `shareimage` varchar(100) NOT NULL,
  `sharedby` int(100) NOT NULL,
  `sharedbyName` varchar(50) NOT NULL,
  `DateofSubmission` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Share`
--

INSERT INTO `Share` (`id`, `user_id`, `shareimage`, `sharedby`, `sharedbyName`, `DateofSubmission`) VALUES
(12, 16, 'bnr15814290141.jpg', 53, '', '0000-00-00 00:00:00'),
(13, 18, 'bnr46937358Koala.jpg', 53, 'Deepak', '0000-00-00 00:00:00'),
(14, 53, 'bnr183023836Penguins.jpg', 53, 'Deepak', '0000-00-00 00:00:00'),
(32, 53, 'bnr1316024256Desert.jpg', 53, 'Deepak', '0000-00-00 00:00:00'),
(35, 53, 'bnr183023836Penguins.jpg', 53, 'Deepak', '2016-07-07 00:00:00'),
(37, 53, 'bnr1316024256Desert.jpg', 53, 'Deepak', '2016-07-08 00:00:00'),
(38, 53, 'bnr15814290141.jpg', 53, 'Deepak', '0000-00-00 00:00:00'),
(39, 53, 'bnr15814290141.jpg', 53, 'Deepak', '0000-00-00 00:00:00'),
(40, 53, 'bnr44110309533.jpg', 53, 'Deepak', '0000-00-00 00:00:00'),
(47, 53, 'bnr183023836Penguins.jpg', 53, 'Deepak', '2016-07-08 00:00:00'),
(53, 53, 'bnr2096223912Beach yoga 2.jpg', 0, '', '2016-07-09 06:16:31'),
(52, 53, 'bnr2140711626Kennedy.jpg', 0, '', '2016-07-09 06:15:20'),
(51, 53, 'bnr2140711626Kennedy.jpg', 0, '', '2016-07-09 06:15:20');

-- --------------------------------------------------------

--
-- Table structure for table `test_mysql`
--

CREATE TABLE `test_mysql` (
  `id` int(4) NOT NULL,
  `name` varchar(65) NOT NULL DEFAULT '',
  `lastname` varchar(65) NOT NULL DEFAULT '',
  `email` varchar(65) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_mysql`
--

INSERT INTO `test_mysql` (`id`, `name`, `lastname`, `email`) VALUES
(1, 'Billly', 'Blueton', 'diveshk442@gmail.com'),
(2, 'Jame', 'Campbell', 'divesh20july@gmail.com'),
(6, 'Divesh', 'Kumar', 'divesh@rankmantra.in');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL,
  `country` varchar(20) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `Lname` varchar(20) DEFAULT NULL,
  `Mname` varchar(20) DEFAULT NULL,
  `cp_number` varchar(20) DEFAULT NULL,
  `Fname` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `dp` varchar(255) DEFAULT NULL,
  `DOB` varchar(20) DEFAULT NULL,
  `Address` text DEFAULT NULL,
  `BusinessName` varchar(50) DEFAULT NULL,
  `BusinessNumber` varchar(50) DEFAULT NULL,
  `TypeofIndustry` varchar(50) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `Accountnumber` varchar(100) DEFAULT NULL,
  `Category` varchar(50) DEFAULT NULL,
  `status` int(255) DEFAULT 0,
  `activation` varchar(255) DEFAULT NULL,
  `oauth_provider` varchar(255) DEFAULT NULL,
  `oauth_uid` varchar(255) DEFAULT NULL,
  `gpluslink` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `country`, `city`, `state`, `zipcode`, `Lname`, `Mname`, `cp_number`, `Fname`, `Email`, `dp`, `DOB`, `Address`, `BusinessName`, `BusinessNumber`, `TypeofIndustry`, `Password`, `time`, `Accountnumber`, `Category`, `status`, `activation`, `oauth_provider`, `oauth_uid`, `gpluslink`, `picture`, `created`, `modified`) VALUES
(70, '', '', '', '', 'Nautiyal', '', '8979528858', 'Deepak', 'Deepaknautiyal52@gmail.com', '', '', '', '', '', '', 'b5ea5f421cbb97b60a7f3cacd82203d1', '', '', '', 1, 'CLIENTS56280338', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, NULL, NULL, NULL, NULL, 'Yu', NULL, '356773324', 'Albert', 'asy@parkingbees.com', NULL, NULL, NULL, NULL, NULL, NULL, '6422d4c8d95a445eef468a9f251e80d9', NULL, NULL, NULL, 1, 'CLIENTS143694108', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, NULL, NULL, NULL, NULL, 'Yu', NULL, '178464659', 'Albert', 'asy@asyadesign.com.ph', NULL, NULL, NULL, NULL, NULL, NULL, '6422d4c8d95a445eef468a9f251e80d9', NULL, NULL, NULL, 0, 'CLIENTS195721891', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1746620125364351, NULL, NULL, NULL, NULL, 'remez', NULL, '12345667', 'ofir', 'ofirremez@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'afdd0b4ad2ec172c586e2150770fbf9e', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, NULL, NULL, NULL, NULL, 'Javier', NULL, '3915373', 'Benjie', 'benjie@parkingbees.com', 'dp2112522129new_dppp.jpg', NULL, NULL, NULL, NULL, NULL, '776a2afcfd0338557e58be955df24b31', NULL, NULL, NULL, 1, 'CLIENTS79365589', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, NULL, NULL, NULL, NULL, 'Corpos', NULL, '175399207', 'Glenn', 'nelgria@gmail.com', 'Client_dp1383718108FB_IMG_1472862886627.jpg', NULL, NULL, NULL, NULL, NULL, 'ea33e0ecde28b02b1168378b1f78333c', NULL, NULL, NULL, 1, 'CLIENTS109215509', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, NULL, NULL, NULL, NULL, 'Puno', NULL, '159928524', 'Michaella', 'michaellapuno24@gmail.com', 'Client_dp932473543IMG_7418.JPG', NULL, NULL, NULL, NULL, NULL, '07ac3c88f442933e87b94be76c409ebd', NULL, NULL, NULL, 1, 'CLIENTS10111465', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1200864769981173, NULL, NULL, NULL, NULL, 'Benjie', NULL, '9081250030', 'Javier', 'benogs@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, 'e64b78fc3bc91bcbc7dc232ba8ec59e0', NULL, NULL, NULL, 1, 'CLIENTS183745556', NULL, NULL, NULL, NULL, NULL, NULL),
(1609433736018506, NULL, NULL, NULL, NULL, 'Javier Jr', NULL, '9081250030', 'Benigno ', 'benjie@ncomputingph.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'CLIENTS88883137', NULL, NULL, NULL, NULL, NULL, NULL),
(1746620125364349, NULL, NULL, NULL, NULL, 'Villaroman', NULL, '09176503421', 'Kayla', 'kayla.villaroman@asyadesign.com.ph', NULL, NULL, NULL, NULL, NULL, NULL, 'd33f5523ed22ce2c220d3d6522d3469b', NULL, NULL, NULL, 0, 'CLIENTS92975051', NULL, NULL, NULL, NULL, NULL, NULL),
(1746620125364350, NULL, NULL, NULL, NULL, 'Villaroman', NULL, '09176503421', 'Kayla', 'kaylamvillaroman@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'b81cd3730c6ef49da07588857fa78d3a', NULL, NULL, NULL, 0, 'CLIENTS31393340', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `gpluslink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `oauth_provider`, `oauth_uid`, `fname`, `lname`, `email`, `gender`, `locale`, `gpluslink`, `picture`, `created`, `modified`) VALUES
(1, 'google', '115201740130498043594', 'Hitesh', 'Rawat', 'hiteshrawat84@gmail.com', 'male', 'en-GB', 'male', 'en-GB', '2016-12-28 11:54:55', '2016-12-29 12:50:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Airportparking`
--
ALTER TABLE `Airportparking`
  ADD PRIMARY KEY (`airid`);

--
-- Indexes for table `amenities_ico`
--
ALTER TABLE `amenities_ico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookparking`
--
ALTER TABLE `bookparking`
  ADD PRIMARY KEY (`bookingid`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_dp`
--
ALTER TABLE `client_dp`
  ADD PRIMARY KEY (`cli_ed_id`);

--
-- Indexes for table `client_google_account`
--
ALTER TABLE `client_google_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clie_google_acc`
--
ALTER TABLE `clie_google_acc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `edit_img_partner`
--
ALTER TABLE `edit_img_partner`
  ADD PRIMARY KEY (`edit_id`);

--
-- Indexes for table `fileimages`
--
ALTER TABLE `fileimages`
  ADD PRIMARY KEY (`imageid`);

--
-- Indexes for table `fileonlyimages`
--
ALTER TABLE `fileonlyimages`
  ADD PRIMARY KEY (`imageid`);

--
-- Indexes for table `fillevent`
--
ALTER TABLE `fillevent`
  ADD PRIMARY KEY (`eventid`);

--
-- Indexes for table `newmember`
--
ALTER TABLE `newmember`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_park`
--
ALTER TABLE `new_park`
  ADD KEY `saerid` (`saerid`);

--
-- Indexes for table `Parkingbysale`
--
ALTER TABLE `Parkingbysale`
  ADD PRIMARY KEY (`parkingid`),
  ADD KEY `salerid` (`salerid`);

--
-- Indexes for table `partn_dp`
--
ALTER TABLE `partn_dp`
  ADD PRIMARY KEY (`par_ed_id`);

--
-- Indexes for table `sellparkinguser`
--
ALTER TABLE `sellparkinguser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Share`
--
ALTER TABLE `Share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_mysql`
--
ALTER TABLE `test_mysql`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `Airportparking`
--
ALTER TABLE `Airportparking`
  MODIFY `airid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `amenities_ico`
--
ALTER TABLE `amenities_ico`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookparking`
--
ALTER TABLE `bookparking`
  MODIFY `bookingid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `client_dp`
--
ALTER TABLE `client_dp`
  MODIFY `cli_ed_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `client_google_account`
--
ALTER TABLE `client_google_account`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `clie_google_acc`
--
ALTER TABLE `clie_google_acc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `edit_img_partner`
--
ALTER TABLE `edit_img_partner`
  MODIFY `edit_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fileimages`
--
ALTER TABLE `fileimages`
  MODIFY `imageid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `fileonlyimages`
--
ALTER TABLE `fileonlyimages`
  MODIFY `imageid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fillevent`
--
ALTER TABLE `fillevent`
  MODIFY `eventid` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `newmember`
--
ALTER TABLE `newmember`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Parkingbysale`
--
ALTER TABLE `Parkingbysale`
  MODIFY `parkingid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11932;

--
-- AUTO_INCREMENT for table `partn_dp`
--
ALTER TABLE `partn_dp`
  MODIFY `par_ed_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sellparkinguser`
--
ALTER TABLE `sellparkinguser`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `Share`
--
ALTER TABLE `Share`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `test_mysql`
--
ALTER TABLE `test_mysql`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1746620125364352;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
